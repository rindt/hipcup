// Generated code from Butter Knife. Do not modify!
package com.lesmtech.hipcup.view;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class ShopsView$$ViewInjector {
  public static void inject(Finder finder, final com.lesmtech.hipcup.view.ShopsView target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624245, "field 'lv_shops' and method 'onItemClickGoods'");
    target.lv_shops = (android.widget.ListView) view;
    ((android.widget.AdapterView<?>) view).setOnItemClickListener(
      new android.widget.AdapterView.OnItemClickListener() {
        @Override public void onItemClick(
          android.widget.AdapterView<?> p0,
          android.view.View p1,
          int p2,
          long p3
        ) {
          target.onItemClickGoods(p2);
        }
      });
  }

  public static void reset(com.lesmtech.hipcup.view.ShopsView target) {
    target.lv_shops = null;
  }
}
