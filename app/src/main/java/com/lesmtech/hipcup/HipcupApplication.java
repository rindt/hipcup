package com.lesmtech.hipcup;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;

import io.fabric.sdk.android.Fabric;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * @author Rindt
 * @version 0.1
 * @since 6/14/15
 */
public class HipcupApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());
        // AVOSCloud.useAVCloudUS();

        AVOSCloud.initialize(this, "66pp69s8u3po86cot7tok1os2yicat1jz8mckmk7cftjwfxs", "psufv2vsn18trpwmjpycd3hxzqv3ajuikhnampc0h0x7se9g");
        // Facebook initialize

        FacebookSdk.sdkInitialize(getApplicationContext());

        // get hash key for configuration
        getHashKey();

//        updateCoffeeShop();

        // testCloudCode();

    }

    private void updateCoffeeShop() {
        try {
            AVQuery<AVObject> query = new AVQuery<>("CoffeeShop");
            query.findInBackground(new FindCallback<AVObject>() {
                @Override
                public void done(List<AVObject> list, AVException e) {
                    for (AVObject object : list) {
                        object.put("description", "Top One Restaurant in HongKong");
                        object.saveInBackground();
                    }
                }
            });
        } catch (Exception e) {

        }
    }


    private void getHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.lesmtech.hipcup",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

}
