package com.lesmtech.hipcup;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * This is to show coupon including the coupon hasn't redeemed or the coupon has redeemed
 * @author Rindt
 * @version 0.1
 * @since 7/2/15
 */
public class CouponActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon);
    }
}
