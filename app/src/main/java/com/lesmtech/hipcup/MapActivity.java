package com.lesmtech.hipcup;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.ui.IconGenerator;
import com.lesmtech.hipcup.entity.Shop;
import com.lesmtech.hipcup.tool.ShopContainer;
import com.lesmtech.hipcup.view.MapInfoWindow;

import java.util.List;

/**
 * This fragment is used to show exactly address in the google map.
 * It can show all shops in the map, also can show a certain that user selected.
 *
 * @author Rindt
 * @version 0.1
 * @since 6/14/15
 */
public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    /**
     * Single Location or MultipleF Location
     */
    public static final String SOURCE = "source";

    public static final int SINGLE = 0;

    public static final int MULTIPLE = 1;

    // For single

    public static final String SHOP = "shop";

    public static final String LAT = "lat";

    public static final String LON = "lon";

    public static final String ID = "id";

    public static final String NAME = "name";

    public static final String ADDRESS = "address";

    public static final String LOGO = "logo";

    /**
     * type = 0 - single
     * type = 1 - multiple
     */
    private int type;

    public static final String VALUE = "value";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        type = getIntent().getIntExtra(SOURCE, -1);
    }

    /**
     * @param googleMap
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Move Camera

        List<Shop> mShops = ShopContainer.getInstance().getShops();

        if (mShops.size() == 0) {
            return;
        }

        if (type == 1) {
            dealWithMutipleShops(googleMap, mShops);
        } else if (type == 0) {
            Bundle data = getIntent().getBundleExtra(SHOP);
            if (data != null) {
                Shop shop = new Shop();
                shop.setId(data.getString(ID));
                shop.setLat(data.getDouble(LAT));
                shop.setLon(data.getDouble(LON));
                shop.setShopAddress(data.getString(ADDRESS));
                shop.setShopName(data.getString(NAME));
                shop.setShopLogoUrl(data.getString(LOGO));
                dealWithSingleShop(googleMap, shop);
            }
        }

    }

    // size >= 1
    private void dealWithMutipleShops(GoogleMap googleMap, final List<Shop> mShops) {

        // Set camera
        Shop shop = mShops.get(0);

        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(shop.getLat(), shop.getLon()), 16));

        // haven't test
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                System.out.println(marker.getTitle());
                View mapInfoView = new MapInfoWindow(MapActivity.this, mShops.get(Integer.parseInt(marker.getTitle())));
                return mapInfoView;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });

        Bitmap bitmap = generateIcon();

        for (int i = 0; i < mShops.size(); i++) {

            Shop item = mShops.get(i);

            googleMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
                    .anchor(0.0f, 1.0f) // Anchors the marker on the bottom left
                    .position(new LatLng(item.getLat(), item.getLon())).title(String.valueOf(i)));
            Log.d("DISTANCE", String.valueOf(SphericalUtil.computeDistanceBetween(new LatLng(39.889, -84.622), new LatLng(40.889, -89.622))));
        }
    }

    private void dealWithSingleShop(GoogleMap googleMap, final Shop mShop) {
        // Set camera
        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(mShop.getLat(), mShop.getLon()), 16));

        // haven't test
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                System.out.println(marker.getTitle());
                View mapInfoView = new MapInfoWindow(MapActivity.this, mShop);
                return mapInfoView;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });

        Bitmap bitmap = generateIcon();

        googleMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
                .anchor(0.0f, 1.0f) // Anchors the marker on the bottom left
                .position(new LatLng(mShop.getLat(), mShop.getLon())).title(String.valueOf(mShop.getId())));
        Log.d("DISTANCE", String.valueOf(SphericalUtil.computeDistanceBetween(new LatLng(39.889, -84.622), new LatLng(40.889, -89.622))));


    }

    private Bitmap generateIcon() {
        // You can customize the marker image using images bundled with
        // your app, or dynamically generated bitmaps.
        IconGenerator iconGenerator = new IconGenerator(this);
        iconGenerator.setBackground(getResources().getDrawable(R.drawable.marker_drop));
        View view = LayoutInflater.from(this).inflate(R.layout.view_marker, null);
//        TextView name = (TextView) view.findViewById(R.id.name);
//        name.setText("Sydney");
        iconGenerator.setContentView(view);
        return iconGenerator.makeIcon();
    }
}
