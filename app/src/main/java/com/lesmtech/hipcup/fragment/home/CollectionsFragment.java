package com.lesmtech.hipcup.fragment.home;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lesmtech.hipcup.R;
import com.lesmtech.hipcup.adapter.ShopCollectionsAdapter;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * This is to show the collection of restaurants
 *
 * @author Rindt
 * @version 0.1
 * @since 6/25/15
 */
public class CollectionsFragment extends Fragment {


    private Activity mActivity;

    @InjectView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;

    RecyclerView.Adapter mAdapter;

    RecyclerView.LayoutManager mLayoutManager;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_collection, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);

        myRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(mActivity);
        myRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        myRecyclerView.setAdapter(mAdapter);
    }


}
