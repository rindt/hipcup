package com.lesmtech.hipcup.fragment.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lesmtech.hipcup.R;

import butterknife.ButterKnife;

/**
 * This fragment is to show good detail including info good detail and the owner shop
 *
 * @author Rindt
 * @version 0.1
 * @since 6/28/15
 */
public class GoodDetailFragment extends Fragment {

    public static final String GOODID = "good_id";

    private String goodID;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        goodID = (String) (getArguments().get(GOODID));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_good_detail, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);
    }
}
