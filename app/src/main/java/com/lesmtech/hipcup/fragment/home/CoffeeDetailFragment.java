package com.lesmtech.hipcup.fragment.home;


import android.support.v4.app.Fragment;

/**
 * This fragment is used to show different coffee various in a certain coffee shop,
 * and let user to select his or her preference.
 *
 * @author Rindt
 * @version 0.1
 * @since 6/14/15
 */
public class CoffeeDetailFragment extends Fragment {
}
