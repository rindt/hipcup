package com.lesmtech.hipcup.fragment.detail;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.lesmtech.hipcup.R;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * @author Rindt
 * @version 0.1
 * @since 7/12/15
 */
public class ContentFragment extends Fragment {

    public final static String BACKGROUND = "background";

    @InjectView(R.id.iv_background)
    ImageView background;

    private Activity mActivity;

    private String bgUrl;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bgUrl = getArguments().getString(BACKGROUND);
        System.out.println(bgUrl);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_content, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);
        Picasso.with(mActivity).load(bgUrl).into(background);
    }
}
