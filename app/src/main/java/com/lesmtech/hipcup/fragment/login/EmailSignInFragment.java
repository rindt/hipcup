package com.lesmtech.hipcup.fragment.login;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.SignUpCallback;
import com.lesmtech.hipcup.BoardingActivity;
import com.lesmtech.hipcup.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * @author Rindt
 * @version 0.1
 * @since 7/5/15
 */
public class EmailSignInFragment extends Fragment {

    @InjectView(R.id.email)
    EditText email;

    @InjectView(R.id.password)
    EditText password;

    @InjectView(R.id.full_name)
    EditText fullName;

    private Activity mActivity;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_emailsignin, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);
    }

    @OnClick(R.id.continueSignIn)
    public void continueSignIn() {
        //  Register a user from server, gets request back and send to email check fragment ;
        String emailValue = email.getText().toString();
        String fullNameValue = fullName.getText().toString();
        String passwordValue = password.getText().toString();
        String error = checkJoiningUserModel(emailValue, passwordValue, fullNameValue);
        if (error == null) {
            // register in the server
            AVUser user = new AVUser();
            user.setUsername(emailValue);
            user.setEmail(emailValue);
            user.setPassword(passwordValue);
            // Method like AVObject, just put different key-value
            user.put("fullName", fullNameValue);
            user.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(AVException e) {
                    if (e == null) {
                        Toast.makeText(mActivity, "Welcome!", Toast.LENGTH_SHORT).show();
                        ((BoardingActivity) mActivity).getSupportFragmentManager()
                                .beginTransaction().replace(R.id.content, new CheckEmailFragment(), null).addToBackStack(null).commit();
                    } else {
                        Toast.makeText(mActivity, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            });

        } else {
            Toast.makeText(mActivity, error, Toast.LENGTH_SHORT).show();
        }
    }

    private String checkJoiningUserModel(String email, String password, String fullName) {
        if (email == null) {
            return "Oops! Please enter your email";
        } else {
            // has a email, but not valid


        }
        if (password == null) {
            return "Oops! Please enter your password";
        } else {
            // Has a password, but not valid


        }
        if (fullName == null) {
            return "Oops!Please enter your full name";
        }
        return null;
    }
}
