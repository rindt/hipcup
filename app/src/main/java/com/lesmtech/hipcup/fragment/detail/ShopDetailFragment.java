package com.lesmtech.hipcup.fragment.detail;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.lesmtech.hipcup.DetailActivity;
import com.lesmtech.hipcup.GoodsListActivity;
import com.lesmtech.hipcup.MapActivity;
import com.lesmtech.hipcup.R;
import com.lesmtech.hipcup.entity.Good;
import com.lesmtech.hipcup.entity.Shop;
import com.lesmtech.hipcup.tool.HGson;
import com.lesmtech.hipcup.view.CouponView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.w3c.dom.Text;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnPageChange;

/**
 * This fragment is to show shop detail including info like goods at the shop and shop info
 *
 * @author Rindt
 * @version 0.1
 * @since 6/28/15
 */
public class ShopDetailFragment extends Fragment {

    //    // add dot in the layout, show the number of view pager
//    @InjectView(R.id.progress_container)
//    LinearLayout progrossContainer;

    @InjectView(R.id.parent)
    CoordinatorLayout parentLayout;

    // create fragment adapter to show frgament in the pager view
    @InjectView(R.id.shop_content_pager)
    ViewPager contentPager;

    @InjectView(R.id.backgrounds)
    ImageSwitcher imageSwitcher;

    private int numberOfImage;

    @InjectView(R.id.toolbar)
    Toolbar mToolbar;

    @InjectView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;

    @InjectView(R.id.fab)
    FloatingActionButton fab;

    @InjectView(R.id.open_time)
    TextView openTime;

    private boolean stateOfFab = true;


//    // Name of the Shop
//    @InjectView(R.id.shopName)
//    TextView shopName;
//
//    // show shop location in the map activity
//    @InjectView(R.id.btn_map)
//    ImageView btn_map;
//
//    // Show detail, haven't decided to use dialog or another fragment to show open time
//    @InjectView(R.id.btn_info)
//    ImageView btn_info;
//
//    @InjectView(R.id.shop_background)
//    ImageView shopBg;

//    @InjectView(R.id.btn_number_love)
//    TextView numberOfLike;

    // Add first four coupon in this container
    @InjectView(R.id.coupons_container)
    LinearLayout conponsContainer;

    private Activity mActivity;

    private Shop mShop;

    private List<Good> mGoods;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    public static final String SHOPID = "shop_id";

    private String shopID;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        shopID = (String) (getArguments().get(SHOPID));
        mShop = new Shop();
        mGoods = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shopdetail, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);

        setImageSwitch();

        setFabListener();
        // get data from server
        getDataFromServer();
    }

    // Update to server, need to initial the fab status
    private void setFabListener() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stateOfFab) {
                    fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
                    // DO something when click "done"
                    // Add shop to collections in user table
                    AVQuery<AVObject> postQuery = new AVQuery<>("CoffeeShop");
                    postQuery.getInBackground(mShop.getId(), new GetCallback<AVObject>() {
                        @Override
                        public void done(AVObject avObject, AVException e) {
                            if (e == null) {
                                AVUser user = AVUser.getCurrentUser();
                                AVRelation<AVObject> relation = user.getRelation("shopLike");
                                relation.add(avObject);
                                user.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(AVException e) {
                                        if (e == null) {
                                            System.out.println("success");
                                            Snackbar.make(parentLayout, "Collected", Snackbar.LENGTH_SHORT).setAction("DONE", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {

                                                }
                                            }).show();
                                            mShop.setHasLike(true);

                                        } else {
                                            System.out.println(e.getMessage());
                                        }
                                    }
                                });
                            } else {
                                System.out.println(e.getMessage());
                            }
                        }
                    });
                    stateOfFab = false;
                } else {
                    // Add shop to collections in user table
                    AVQuery<AVObject> postQuery = new AVQuery<>("CoffeeShop");
                    postQuery.getInBackground(mShop.getId(), new GetCallback<AVObject>() {
                        @Override
                        public void done(AVObject avObject, AVException e) {
                            if (e == null) {
                                AVUser user = AVUser.getCurrentUser();
                                AVRelation<AVObject> relation = user.getRelation("shopLike");
                                relation.remove(avObject);
                                user.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(AVException e) {
                                        if (e == null) {
                                            System.out.println("success");
                                            fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp));
                                            Snackbar.make(parentLayout, "Removed", Snackbar.LENGTH_SHORT).setAction("DONE", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    // DO something when click "done"

                                                }
                                            }).show();
                                            mShop.setHasLike(false);
                                        } else {
                                            System.out.println(e.getMessage());
                                        }
                                    }
                                });
                            } else {
                                System.out.println(e.getMessage());
                            }
                        }
                    });


                    stateOfFab = true;
                }
            }
        });
    }

    public void createCouponLayout() {

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();

        int numberInLayout = 0;

        LinearLayout container = null;

        for (int i = 0; i < mGoods.size(); i++) {
            CouponView couponView;
            if (i % 2 == 0) {
                container = new LinearLayout(mActivity);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height / 4);
                params.setMargins(0, 10, 0, 10);
                container.setOrientation(LinearLayout.HORIZONTAL);
                container.setGravity(Gravity.CENTER);
                container.setLayoutParams(params);
            }
            FrameLayout.LayoutParams param = new FrameLayout.LayoutParams(width / 2, ViewGroup.LayoutParams.WRAP_CONTENT);
            param.setMargins(5, 0, 5, 0);
            // Test if have 4 coupons
            couponView = new CouponView(getActivity(), mGoods.get(i));
            couponView.setLayoutParams(param);
            container.addView(couponView);
            numberInLayout++;
            if (numberInLayout == 2) {
                conponsContainer.addView(container);
                numberInLayout = 0;
            }
        }
    }

    private void getDataFromServer() {
        // Get shop detail;
        AVQuery<AVObject> query = new AVQuery<>("CoffeeShop");
        query.getInBackground(shopID, new GetCallback<AVObject>() {
            @Override
            public void done(AVObject object, AVException e) {
                mShop = HGson.getInstance().fromJson(object.toJSONObject().toString(), Shop.class);
                mShop.setId(object.getObjectId());
                mShop.setShopLogoUrl(object.getAVFile("shopLogo").getUrl());
                mShop.setShopBgUrl(object.getAVFile("shopView").getUrl());
                mShop.setLat(object.getAVGeoPoint("shopGeoPoint").getLatitude());
                mShop.setLon(object.getAVGeoPoint("shopGeoPoint").getLongitude());
                mShop.setNumberOfLike(object.getInt("numberOfLike"));
                // Loading data from server response of shop
                renderShopData();

                // Search first 4 coupon based on the shop
                AVRelation<AVObject> goods = object.getRelation("goods");
                AVQuery<AVObject> query = goods.getQuery();
                query.setLimit(4);
                query.findInBackground(new FindCallback<AVObject>() {
                    @Override
                    public void done(List<AVObject> list, AVException e) {
                        if (e == null) {
                            for (AVObject object : list) {
                                Good good = HGson.getInstance().fromJson(object.toJSONObject().toString(), Good.class);
                                good.setGoodUrl(object.getAVFile("pic").getUrl());
                                mGoods.add(good);
                            }
                            // Create Coupon Layout using parsed Goods
                            createCouponLayout();
                            setFeatureGoods();
                            setViewPager();
                        } else {
                            Toast.makeText(mActivity, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });
    }

    public void setViewPager() {
        contentPager.setAdapter(new ShopContentFragmentAdapter(((DetailActivity) mActivity).getSupportFragmentManager()));
    }

    public class ShopContentFragmentAdapter extends FragmentPagerAdapter {

        public ShopContentFragmentAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // Add different content here
            Fragment contentFragment = new ContentFragment();
            Bundle bundle = new Bundle();
            bundle.putString(ContentFragment.BACKGROUND, mShop.getShopBgUrl());
            contentFragment.setArguments(bundle);
            return contentFragment;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }


    public void renderShopData() {
        String[] openHour = mShop.getShopOpenHour().split(";");
        StringBuilder result = new StringBuilder();
        for (String day : openHour) {
            day = day.trim();
            result.append(day).append("\n");
        }
        openTime.setText(result.substring(0, result.length() - 1));

        renderToolbarNTabColor(mShop.getShopBgUrl());
        collapsingToolbarLayout.setTitle(mShop.getShopName());
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
//        shopName.setText(mShop.getShopName());
//        numberOfLike.setText(String.valueOf(mShop.getNumberOfLike()));
//        Picasso.with(mActivity).load(mShop.getShopBgUrl()).into(shopBg);
    }

    private void renderToolbarNTabColor(String bgUrl) {
        new BitmapFromUrl().execute(bgUrl);
    }

    public class BitmapFromUrl extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... params) {
            URL url = null;
            try {
                url = new URL(params[0]);
                return BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (bitmap != null) {
                Palette.generateAsync(bitmap, new Palette.PaletteAsyncListener() {
                    @Override
                    public void onGenerated(Palette palette) {
                        Palette.Swatch swatch = palette.getVibrantSwatch();
                        int rgbColor = swatch.getRgb();
                        int titleTextColor = swatch.getTitleTextColor();
                        int bodyTextColor = swatch.getBodyTextColor();
                        System.out.println(rgbColor + " " + titleTextColor + " " + bodyTextColor);
                        collapsingToolbarLayout.setExpandedTitleColor(titleTextColor);
                        collapsingToolbarLayout.setCollapsedTitleTextColor(titleTextColor);
//                        fab.setBackgroundTintList(ColorStateList.valueOf(bodyTextColor));
                    }
                });
            } else {
                // default
            }
        }
    }

    // add dots in the view.
    @OnPageChange(R.id.shop_content_pager)
    public void onPageChange(int position) {
        System.out.println(position);


    }

    // This click is going to show all coupon the shop holds
    @OnClick(R.id.btn_view_all)
    public void btnViewAll() {
        Intent showAllGoods = new Intent(mActivity, GoodsListActivity.class);
        showAllGoods.putExtra(SHOPID, shopID);
        startActivity(showAllGoods);
    }

    @OnClick(R.id.action_back)
    public void btnActionBack() {
        mActivity.onBackPressed();
    }

    @OnClick(R.id.action_map)
    public void btnMap() {
        // start the map activity
        Intent mapIntent = new Intent(mActivity, MapActivity.class);
        mapIntent.putExtra(MapActivity.SOURCE, MapActivity.SINGLE);
        Bundle shop = new Bundle();
        shop.putString(MapActivity.ID, mShop.getId());
        shop.putString(MapActivity.NAME, mShop.getShopName());
        shop.putString(MapActivity.LOGO, mShop.getShopLogoUrl());
        shop.putDouble(MapActivity.LAT, mShop.getLat());
        shop.putDouble(MapActivity.LON, mShop.getLon());
        shop.putString(MapActivity.ADDRESS, mShop.getShopAddress());
        mapIntent.putExtra(MapActivity.SHOP, shop);
        startActivity(mapIntent);
    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            imageSwitcher.setImageDrawable(new BitmapDrawable(bitmap));
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

    @OnClick(R.id.prev_food)
    public void prevFood() {
        if (numberOfImage != 0) {
            Picasso.with(mActivity).load(mGoods.get(--numberOfImage).getGoodUrl()).into(target);
        } else {

        }
    }

    @OnClick(R.id.next_food)
    public void nextFood() {
        if (numberOfImage != mGoods.size() - 1 && mGoods.size() != 0) {
            Picasso.with(mActivity).load(mGoods.get(++numberOfImage).getGoodUrl()).into(target);
        }
    }

    private void setImageSwitch() {
        Animation in = AnimationUtils.loadAnimation(mActivity, android.R.anim.fade_in);
        Animation out = AnimationUtils.loadAnimation(mActivity, android.R.anim.fade_out);
        imageSwitcher.setInAnimation(in);
        imageSwitcher.setOutAnimation(out);
        imageSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView image = new ImageView(mActivity);
                image.setScaleType(ImageView.ScaleType.FIT_XY);
                image.setLayoutParams(new ImageSwitcher.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                return image;
            }
        });
    }

    private void setFeatureGoods() {
        if (mGoods != null && mGoods.size() != 0) {
            Picasso.with(mActivity).load(mGoods.get(0).getGoodUrl()).into(target);
        }
    }

    /**
     * Redirect to Good Detail Screen using numberOfImage to indicate the number of good in the good list
     */
    @OnClick(R.id.backgrounds)
    public void directToGoodDetail() {

    }

}
