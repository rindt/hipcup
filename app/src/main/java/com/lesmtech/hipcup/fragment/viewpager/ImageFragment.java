package com.lesmtech.hipcup.fragment.viewpager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.lesmtech.hipcup.MapActivity;
import com.lesmtech.hipcup.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * @author Rindt
 * @version 0.1
 * @since 7/24/15
 */
public class ImageFragment extends Fragment {

    public final static String IMAGE_URL = "imageUrl";

    @InjectView(R.id.image)
    ImageView image;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_image, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ButterKnife.inject(this, view);
        String imageURL = getArguments().getString(IMAGE_URL);
        if (imageURL != null) {
            Picasso.with(getActivity()).load(imageURL).into(image, new Callback() {
                @Override
                public void onSuccess() {
                    System.out.println("Success Loading Image");
                }

                @Override
                public void onError() {
                    System.out.println("Error Loading Image");
                }
            });
        }
    }
}
