package com.lesmtech.hipcup.fragment.home;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.lesmtech.hipcup.R;
import com.lesmtech.hipcup.entity.Shop;
import com.lesmtech.hipcup.tool.HGson;
import com.lesmtech.hipcup.view.ShopListItemView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * This fragment is used to show the list of Coffee shop ordered by distance
 *
 * @author Rindt
 * @version 0.1
 * @since 6/14/15
 */
public class GoodsFragment extends Fragment {

    @InjectView(R.id.lv_shops)
    ListView lv_shops;

    private Activity mActivity;

    private List<Shop> coffeeShops;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_shop, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        // Initial ListView
        coffeeShops = new ArrayList<>();
        requestCoffeeShops();
        renderView();
    }

    public void requestCoffeeShops() {
        AVQuery<AVObject> query = new AVQuery<>("CoffeeShop");
        query.setLimit(20);
        query.findInBackground(new FindCallback<AVObject>() {
            public void done(List<AVObject> avObjects, AVException e) {
                if (e == null) {
                    for (AVObject object : avObjects) {
                        Shop shop = HGson.getInstance().fromJson(object.toJSONObject().toString(), Shop.class);
                        shop.setShopLogoUrl(object.getAVFile("shopLogo").getUrl());
                        shop.setShopBgUrl(object.getAVFile("shopView").getUrl());
                        coffeeShops.add(shop);
                    }
                    Log.d("成功", "查询到" + avObjects.toString() + " 条符合条件的数据");

                    if(lv_shops.getAdapter() == null) {
                        lv_shops.setAdapter(shopsAdapter);
                    }else{
                        shopsAdapter.notifyDataSetChanged();
                    }
                } else {
                    Log.d("失败", "查询错误: " + e.getMessage());
                }
            }
        });
    }

    public void renderView() {

    }

    public BaseAdapter shopsAdapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return coffeeShops.size();
        }

        @Override
        public Object getItem(int position) {
            return coffeeShops.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = new ShopListItemView(mActivity, (Shop) getItem(position));
            } else {
                ((ShopListItemView) convertView).setCoffeeShop((Shop) getItem(position));
            }

            // Load more data

            return convertView;
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }
}
