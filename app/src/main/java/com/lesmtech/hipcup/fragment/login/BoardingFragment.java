package com.lesmtech.hipcup.fragment.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.LogInCallback;
import com.avos.avoscloud.SignUpCallback;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.lesmtech.hipcup.BoardingActivity;
import com.lesmtech.hipcup.HomeActivity;
import com.lesmtech.hipcup.R;
import com.lesmtech.hipcup.entity.FacebookUser;
import com.lesmtech.hipcup.tool.HGson;

import org.json.JSONObject;

import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * @author Rindt
 * @version 0.1
 * @since 7/5/15
 */
public class BoardingFragment extends Fragment {

    private Activity mActivity;

    @InjectView(R.id.facebook_login)
    LoginButton facebookLogInButton;

    private CallbackManager mCallbackManager;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        System.out.println("success");
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        System.out.println("cancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        System.out.println("error");
                    }
                });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_boarding, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);
        facebookLogInButton.setReadPermissions(Arrays.asList("public_profile", "user_friends", "email", "user_birthday"));
        facebookLogInButton.setFragment(this);
        facebookLogInButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                // Application code
                                System.out.println(object.toString());
                                // Check the user
                                FacebookUser user = HGson.getInstance().fromJson(object.toString(), FacebookUser.class);
                                checkUserInServer(user);
                            }
                        });
                request.executeAsync();
                System.out.println("success");
            }

            @Override
            public void onCancel() {
                // App code
                System.out.println("cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                System.out.println(exception.getMessage());
            }
        });

    }


    @OnClick(R.id.email_login)
    public void emailLogIn() {
        ((BoardingActivity) mActivity)
                .getSupportFragmentManager()
                .beginTransaction().replace(R.id.content, new EmailSignInFragment(), null).addToBackStack(null).commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void checkUserInServer(final FacebookUser fbUser) {

        // Check whether the user has registered in the server
        AVUser.logInInBackground(fbUser.getId(), fbUser.getId(), new LogInCallback<AVUser>() {
            public void done(AVUser user, AVException e) {
                if (user != null) {
                    // 登录成功
                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    startActivity(intent);
                    // Animation

                } else {
                    // 登录失败
                    // Register with Facebook id as a username and a password
                    AVUser joiningUser = new AVUser();
                    joiningUser.setUsername(fbUser.getId());
                    joiningUser.setEmail(fbUser.getEmail());
                    joiningUser.setPassword(fbUser.getId());
                    // Method like AVObject, just put different key-value
                    joiningUser.put("fullName", fbUser.getFirst_name() + fbUser.getLast_name());
                    joiningUser.put("gender", fbUser.getGender());
                    joiningUser.put("birthday", fbUser.getBirthday());
                    joiningUser.signUpInBackground(new SignUpCallback() {
                        @Override
                        public void done(AVException e) {
                            if (e == null) {
                                Toast.makeText(mActivity, "Welcome!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(mActivity, HomeActivity.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(mActivity, e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                    });
                }
            }
        });


    }

    private void registerJoiningUser(FacebookUser user) {

    }
}
