package com.lesmtech.hipcup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.LogInCallback;
import com.avos.avoscloud.RequestPasswordResetCallback;
import com.avos.avoscloud.SignUpCallback;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.FacebookServiceException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.lesmtech.hipcup.entity.FacebookUser;
import com.lesmtech.hipcup.entity.JoiningUser;
import com.lesmtech.hipcup.entity.Session;
import com.lesmtech.hipcup.tool.HGson;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * This activity is used to include SignInFragment, SignUpFragment, ForgetPasswordFragment.
 *
 * @author Rindt
 * @version 0.1
 * @since 6/14/15
 */
public class BoardingActivity extends AppCompatActivity {


    // Three states, forgot your password; SignUp; LogIn
    private int LOGIN = 0;

    private int SIGNUP = 1;

    private int FORGET = 2;

    private int state = 0;

    // Indicate whether the corner button is clickable;
    private boolean clickable = true;

    @InjectView(R.id.email)
    EditText email;

    @InjectView(R.id.password_container)
    RelativeLayout passwordContainer;

    @InjectView(R.id.password)
    EditText password;

    @InjectView(R.id.username)
    EditText username;

    @InjectView(R.id.forgot_password)
    TextView forgetPwd;

    @InjectView(R.id.btn_signUpAndLogIn)
    TextView signUpAndLogInButton;

    @InjectView(R.id.switchButton)
    TextView switchButton;

    @InjectView(R.id.parent_view)
    RelativeLayout parentView;

    @InjectView(R.id.btn_facebook_login)
    LoginButton btnFBLogIn;

    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_boarding);
        ButterKnife.inject(this);

        facebookLogInSetUp();

        validateSession();

//        // Animation Background
//        AnimationDrawable animationDrawable = (AnimationDrawable) parentView.getBackground();
//        animationDrawable.start();

        // View
        renderViewByState();
        // Button in toolbar
        changeLogInNSignInButtonState();

    }

    private void facebookLogInSetUp() {

        callbackManager = CallbackManager.Factory.create();

        btnFBLogIn.setReadPermissions("public_profile","user_friends", "email", "user_birthday");

        // Callback registration
        btnFBLogIn.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.d("facebook", "Success");
                Log.d("facebook", loginResult.getAccessToken().getToken());
                Log.d("facebook", AccessToken.getCurrentAccessToken().getToken());
                getFacebookID(loginResult.getAccessToken().getUserId());
            }

            @Override
            public void onCancel() {
                // App code
                Log.d("LogInCallBack", "Cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d("LogInCallBack", "Error");
            }
        });
    }

    @OnClick(R.id.btn_signUpAndLogIn)
    public void btnSignUpLogIn() {
        //check state
        if (state == LOGIN) {
            // send request to login
            System.out.println("LOG IN REQUEST");
            userLogIn();
        } else if (state == SIGNUP) {
            // SIGN UP
            System.out.println("SIGN UP REQUEST");
            signUpUser();
        }
        // Forget Password
        else {
            // Send request to get back password
            System.out.println("FORGET REQUEST");
            requestPassword(email.getText().toString());
        }
    }

    // LogIn & SignUp
    @OnClick(R.id.switchButton)
    public void switchState() {
        if (state == LOGIN) {
            state = SIGNUP;
            switchButton.setText("Log in");
        } else if (state == SIGNUP) {
            state = LOGIN;
            switchButton.setText("Sign in");
        } else if (state == FORGET) {
            state = LOGIN;
            switchButton.setText("Sign in");
        }
        renderViewByState();
        changeLogInNSignInButtonState();
    }

    public void renderViewByState() {
        if (state == LOGIN) {
            username.setVisibility(View.INVISIBLE);
            passwordContainer.setVisibility(View.VISIBLE);
            forgetPwd.setVisibility(View.VISIBLE);
            signUpAndLogInButton.setText("LOG IN");
        } else if (state == SIGNUP) {
            forgetPwd.setVisibility(View.GONE);
            username.setVisibility(View.VISIBLE);
            signUpAndLogInButton.setText("SIGN UP");
        } else {
            // Forget Password;
            passwordContainer.setVisibility(View.INVISIBLE);
            forgetPwd.setVisibility(View.INVISIBLE);
            switchButton.setText("Log in");
            signUpAndLogInButton.setText("SEND");
        }
    }

    private void changeLogInNSignInButtonState() {
        if (state == LOGIN) {
            if (!email.getText().toString().equals("") && !password.getText().toString().equals("")) {
                System.out.println("Login valid");
                //Change corner button color;
                signUpAndLogInButton.setTextColor(getResources().getColor(R.color.white));
                setButtonStatu(true);
            } else {
                System.out.println("Login invalid");
                signUpAndLogInButton.setTextColor(getResources().getColor(R.color.gray));
                setButtonStatu(false);
            }
        } else if (state == SIGNUP) {
            if (!email.getText().toString().equals("") && !password.getText().toString().equals("") && !username.getText().toString().equals("")) {
                System.out.println("Sign Up valid");
                signUpAndLogInButton.setTextColor(getResources().getColor(R.color.white));
                setButtonStatu(true);
            } else {
                System.out.println("Sign Up invalid");
                signUpAndLogInButton.setTextColor(getResources().getColor(R.color.gray));
                setButtonStatu(false);
            }
        } else {
            if (!email.getText().toString().equals("")) {
                System.out.println("Forget valid");
                signUpAndLogInButton.setTextColor(getResources().getColor(R.color.white));
                setButtonStatu(true);
            } else {
                System.out.println("Forget invalid");
                signUpAndLogInButton.setTextColor(getResources().getColor(R.color.gray));
                setButtonStatu(false);
            }
        }
    }

    @OnClick(R.id.forgot_password)
    public void btnForgetPassword() {
        state = FORGET;
        switchButton.setText("Log in");
        renderViewByState();
        changeLogInNSignInButtonState();
    }

    @OnTextChanged({R.id.email, R.id.password, R.id.username})
    public void onBeforeTextChange(CharSequence text) {
        System.out.println("Trigger:" + text);
        changeLogInNSignInButtonState();
    }

    private void setButtonStatu(Boolean able) {
        signUpAndLogInButton.setEnabled(able);
        signUpAndLogInButton.setClickable(able);
    }

    private void signUpUser() {
        final AVUser joiningUser = new AVUser();
        joiningUser.setUsername(email.getText().toString());
        joiningUser.setEmail(email.getText().toString());
        joiningUser.setPassword(password.getText().toString());
        joiningUser.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(AVException e) {
                if (e == null) {
                    // LogIn.. Get AvUser
                    AVUser.logInInBackground(email.getText().toString(), password.getText().toString(), new LogInCallback<AVUser>() {
                        @Override
                        public void done(AVUser avUser, AVException e) {
                            saveUserToPre(avUser);
                            Toast.makeText(BoardingActivity.this, "Welcome!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(BoardingActivity.this, HomeActivity.class);
                            startActivity(intent);
                            BoardingActivity.this.finish();
                        }
                    });
                } else {
                    Toast.makeText(BoardingActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void userLogIn() {
        AVUser.logInInBackground(email.getText().toString(), password.getText().toString(), new LogInCallback<AVUser>() {
            @Override
            public void done(AVUser avUser, AVException e) {
                if (e == null) {
                    // Save avUser data in Pre
                    saveUserToPre(avUser);
                    // To home Activity
                    Toast.makeText(BoardingActivity.this, "Welcome!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(BoardingActivity.this, HomeActivity.class);
                    startActivity(intent);
                    BoardingActivity.this.finish();
                } else {
                    Toast.makeText(BoardingActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void saveUserToPre(AVUser user) {
        SharedPreferences shre = getSharedPreferences("session", MODE_PRIVATE);
        SharedPreferences.Editor editor = shre.edit();
        editor.putString(Session.ID, user.getObjectId());
        editor.putString(Session.EMAIL, user.getEmail());
        editor.putString(Session.BIRTHDAY, user.getString(Session.BIRTHDAY));
        editor.putString(Session.FULLNAME, user.getString(Session.FULLNAME));
        editor.putString(Session.GENDER, user.getString(Session.GENDER));
        editor.putString(Session.PHONENUMBER, user.getMobilePhoneNumber());
        editor.putString(Session.USERNAME, user.getUsername());
        editor.apply();
    }

    private void requestPassword(String email) {
        AVUser.requestPasswordResetInBackground(email, new RequestPasswordResetCallback() {
            public void done(AVException e) {
                if (e == null) {
                    // 已发送一份重置密码的指令到用户的邮箱
                    Toast.makeText(BoardingActivity.this, "Please Check your email!", Toast.LENGTH_SHORT).show();
                } else {
                    // 重置密码出错。
                    Toast.makeText(BoardingActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    @OnClick(R.id.btn_delete)
    public void deleteTextInEmail() {





    }

    @OnClick(R.id.btn_show)
    public void togglePassword(View view) {
        if (password.isSelected()) {
            password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            password.setSelection(password.getText().length());
            view.setBackgroundResource(R.drawable.ic_visibility_off_white_24dp);
            password.setSelected(false);
        } else {
            password.setInputType(InputType.TYPE_CLASS_TEXT);
            password.setSelection(password.getText().length());
            view.setBackgroundResource(R.drawable.ic_visibility_white_24dp);
            password.setSelected(true);
        }

    }


    /**
     * Session check, if logged in, redirect to HomeActivity.class
     */
    private void validateSession() {
        SharedPreferences sharedPreferences = getSharedPreferences("session", MODE_PRIVATE);
        if (sharedPreferences.getString(Session.ID, null) != null) {
            Intent intent = new Intent(BoardingActivity.this, HomeActivity.class);
            startActivity(intent);
            BoardingActivity.this.finish();
        }
    }

    private void getFacebookID(String userId) {
        StringBuilder request = new StringBuilder("/");
        request.append(userId);
        // LogIn...Get GraphResponse
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                request.toString(),
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        if (response == null) {
                            Log.d("facebook", "response == null");
                        } else {
                            FacebookUser fbUser = HGson.getInstance().fromJson(response.getJSONObject().toString(), FacebookUser.class);
                            // Check the email on the server
                            checkUserInServer(fbUser);

                            if (fbUser.getEmail() == null) {
                                // .... how to handle in UI

                            }

                        }
                    }
                }
        ).executeAsync();
    }

    private void checkUserInServer(final FacebookUser fbUser) {
        // Check whether the user has registered in the server
        AVUser.logInInBackground(fbUser.getId(), fbUser.getId(), new LogInCallback<AVUser>() {
            public void done(AVUser user, AVException e) {
                if (user != null) {
                    // 登录成功
                    Intent intent = new Intent(BoardingActivity.this, HomeActivity.class);
                    saveUserToPre(user);
                    startActivity(intent);
                    BoardingActivity.this.finish();
                } else {
                    // 登录失败
                    // Register with Facebook id as a username and a password
                    final AVUser joiningUser = new AVUser();
                    joiningUser.setUsername(fbUser.getId());
                    joiningUser.setEmail(fbUser.getEmail());
                    joiningUser.setPassword(fbUser.getId());
                    // Method like AVObject, just put different key-value
                    joiningUser.put("fullName", fbUser.getFirst_name() + fbUser.getLast_name());
                    joiningUser.put("gender", fbUser.getGender());
                    joiningUser.put("birthday", fbUser.getBirthday());
                    joiningUser.signUpInBackground(new SignUpCallback() {
                        @Override
                        public void done(AVException e) {
                            if (e == null) {
                                Toast.makeText(BoardingActivity.this, "Welcome!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(BoardingActivity.this, HomeActivity.class);
                                saveUserToPre(joiningUser);
                                startActivity(intent);
                                BoardingActivity.this.finish();
                            } else {
                                Toast.makeText(BoardingActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
