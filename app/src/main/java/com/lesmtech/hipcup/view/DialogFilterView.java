package com.lesmtech.hipcup.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.kyleduo.switchbutton.SwitchButton;
import com.lesmtech.hipcup.HomeActivity;
import com.lesmtech.hipcup.R;
import com.lesmtech.hipcup.entity.FilterSortModel;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.SwipeTouchListener;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * This view is used to manager the filter
 *
 * @author Rindt
 * @version 0.1
 * @since 7/3/15
 */
public class DialogFilterView extends FrameLayout {

    private FilterSortModel mFilterSortModel;

    private Context mContext;

    private Dialog dialog;

    private int[] sortTypeId = {R.id.distance, R.id.rating};

    private int selectedSort = -1;

    private int[] distanceTypeId = {R.id.zero, R.id.fiveZero, R.id.ten, R.id.two, R.id.twoFive};

    private int[] priceLevelId = {R.id.price_level_one, R.id.price_level_two, R.id.price_level_three};

    private int selectedDistance = -1;

    private int selectedPriceLevel = -1;

    private boolean wifi = false;

    private boolean hourfree = false;

    @InjectView(R.id.clear)
    TextView clear;

    @InjectView(R.id.done)
    TextView done;

    @InjectView(R.id.distance)
    ToggleButton distance;

    @InjectView(R.id.rating)
    ToggleButton rating;

    @InjectView(R.id.wifi_sb)
    SwitchButton sbWifi;

    @InjectView(R.id.hour_free_sb)
    SwitchButton sbHourFree;

    public DialogFilterView(Context context) {
        super(context);
        mContext = context;
        initView(context);
    }

    public DialogFilterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView(context);
    }

    public DialogFilterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView(context);
    }

    public DialogFilterView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
        initView(context);
    }

    public DialogFilterView(Context context, FilterSortModel model) {
        super(context);
        mContext = context;
        mFilterSortModel = model;
        initView(context);
    }

    private void initView(Context context) {
        // Default FilterSortModel
        // if the model is null, so the user first clicks the filter
        if (mFilterSortModel == null) {
            mFilterSortModel = new FilterSortModel(-1, -1, -1, false, false);
        }
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_filter, null);
        ButterKnife.inject(this, view);
        addView(view);
    }

    public void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }

    @OnClick(R.id.done)
    public void done() {
        // Send back the sort model to home activity, and handle the network request
        mFilterSortModel = new FilterSortModel(selectedSort, selectedDistance, selectedPriceLevel, sbWifi.isChecked(), sbHourFree.isChecked());
        ((HomeActivity) mContext).getSortResult(mFilterSortModel);
        dialog.cancel();
    }

    @OnClick({R.id.distance, R.id.rating})
    public void selectSort(ToggleButton button) {
        for (int id : sortTypeId) {
            if (button.getId() != id) {
                ((ToggleButton) this.findViewById(id)).setChecked(false);
            } else {
                switch (id) {
                    case R.id.distance:
                        selectedSort = 0;
                        break;
                    case R.id.rating:
                        selectedSort = 1;
                        break;
                }
            }
        }
    }

    @OnClick({R.id.zero, R.id.two, R.id.ten, R.id.twoFive, R.id.fiveZero})
    public void selectDistance(ToggleButton button) {
        for (int id : distanceTypeId) {
            if (button.getId() != id) {
                ((ToggleButton) this.findViewById(id)).setChecked(false);
            } else {
                switch (id) {
                    case R.id.zero:
                        selectedDistance = 0;
                        break;
                    case R.id.two:
                        selectedDistance = 1;
                        break;
                    case R.id.ten:
                        selectedDistance = 2;
                        break;
                    case R.id.twoFive:
                        selectedDistance = 3;
                        break;
                    case R.id.fiveZero:
                        selectedDistance = 4;
                        break;
                }
            }
        }
    }

    @OnClick(R.id.lighting)
    public void showLightingOptions() {

    }

    @OnClick(R.id.location)
    public void showLocationOptions() {

    }
//
//    @OnCheckedChanged(R.id.hour_limit_sb)
//    public void hourLimitOnChecked(boolean checked) {
//        if (checked) {
//            mFilterSortModel.setHourLimit(true);
//        } else {
//            mFilterSortModel.setHourLimit(false);
//        }
//    }
//
//    @OnCheckedChanged(R.id.wifi_sb)
//    public void wifiOnChecked(boolean checked) {
//        if (checked) {
//            mFilterSortModel.setWifi(true);
//        } else {
//            mFilterSortModel.setWifi(false);
//        }
//    }

    @OnClick({R.id.price_level_one, R.id.price_level_two, R.id.price_level_three})
    public void priceViewChange(CompoundButton button) {

        int clickedButtonId = button.getId();

        switch (clickedButtonId) {
            case R.id.price_level_one:
                selectedPriceLevel = 0;
                break;
            case R.id.price_level_two:
                selectedPriceLevel = 1;
                break;
            case R.id.price_level_three:
                selectedPriceLevel = 2;
                break;
        }

        for (int i = 0; i < 3; i++) {
            if (i <= selectedPriceLevel) {
                ((ToggleButton) this.findViewById(priceLevelId[i])).setChecked(true);
            }
            else{
                ((ToggleButton) this.findViewById(priceLevelId[i])).setChecked(false);
            }
        }

    }


}
