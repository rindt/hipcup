package com.lesmtech.hipcup.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.lesmtech.hipcup.R;
import com.lesmtech.hipcup.entity.Shop;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * @author Rindt
 * @version 0.1
 * @since 7/9/15
 */
public class MapInfoWindow extends FrameLayout {

    private Context mContext;

    private Shop mShop;

    @InjectView(R.id.name)
    TextView tv_name;

    @InjectView(R.id.logo)
    ImageView iv_logo;

    @InjectView(R.id.btn_go)
    TextView btn_go;

    public MapInfoWindow(Context context) {
        super(context);
        mContext = context;
        initView(context);
    }

    public MapInfoWindow(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView(context);
    }

    public MapInfoWindow(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView(context);
    }

    public MapInfoWindow(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
        initView(context);
    }

    public MapInfoWindow(Context context, Shop shop) {
        super(context);
        mContext = context;
        mShop = shop;
        initView(context);
    }

    private void initView(Context context) {
        addView(LayoutInflater.from(context).inflate(R.layout.view_map_info, null));
        ButterKnife.inject(this);
        if (mShop != null) {
            setShop(mShop);
        }
    }

    private void setShop(Shop shop) {
        tv_name.setText(shop.getShopName());
        Picasso.with(mContext).load(shop.getShopLogoUrl()).into(iv_logo);
    }

    @OnClick(R.id.btn_go)
    public void onClickGo() {
        // Start shop detail activity to shop detail
        System.out.println("Click Go:" + mShop.getId());

    }
}
