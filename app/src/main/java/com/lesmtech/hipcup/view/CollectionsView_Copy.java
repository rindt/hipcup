package com.lesmtech.hipcup.view;

import android.app.Activity;
import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
import com.lesmtech.hipcup.HomeActivity;
import com.lesmtech.hipcup.R;
import com.lesmtech.hipcup.adapter.ShopCollectionsAdapter;
import com.lesmtech.hipcup.entity.Shop;
import com.lesmtech.hipcup.tool.HGson;
import com.lesmtech.hipcup.tool.ShopContainer;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * This view is in the home activity to show user's collections of restaurant.
 *
 * @author Rindt
 * @version 0.1
 * @since 6/25/15
 */
public class CollectionsView_Copy extends SwipeRefreshLayout {

    private Context mContext;

    private List<Shop> mlistCoffeeShop;

    private LatLng currentLocation;

    private boolean hasMoreData;

    @InjectView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;

    RecyclerView.Adapter mAdapter;

    RecyclerView.LayoutManager mLayoutManager;

    private AlphaInAnimationAdapter mAlphaInAnimationAdapter;

    public CollectionsView_Copy(Context context) {
        super(context);
        mContext = context;
        initView();
    }

    public CollectionsView_Copy(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView();
    }

    public void initView() {
        addView(LayoutInflater.from(mContext).inflate(R.layout.view_collection, null));
        ButterKnife.inject(this);

        mlistCoffeeShop = new ArrayList<>();

        myRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(mContext);
        myRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ShopCollectionsAdapter(mlistCoffeeShop, mContext);
        setColorSchemeResources(R.color.blue300, R.color.blue500, R.color.blue700);
        this.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });
        //Get Collections
        requestCollectionsShop(false);

    }

    public void requestCollectionsShop(boolean refresh) {

        currentLocation = ((HomeActivity) mContext).getCurrentLocation();

        AVRelation<AVObject> relation = AVUser.getCurrentUser().getRelation("shopLike");
        AVQuery query = relation.getQuery();

        // 实现分页, 一页20个
        if (!refresh) {
            query.setSkip(mlistCoffeeShop.size());
        } else {
            // Can do better, coz the view will be blank when loading
            mlistCoffeeShop.clear();
        }

        query.setLimit(20);
        query.skip(mlistCoffeeShop.size());

        query.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if (e == null) {
                    // Whether has more data
                    hasMoreData = list.size() == 20;

                    for (AVObject object : list) {

                        Shop shop = HGson.getInstance().fromJson(object.toJSONObject().toString(), Shop.class);
                        // Get id from the object identified the line of data
                        shop.setId(object.getObjectId());
                        if (object.getAVFile("shopLogo") != null) {
                            shop.setShopLogoUrl(object.getAVFile("shopLogo").getUrl());
                        } else {
                            shop.setShopLogoUrl("http://www.theamazingspidermangame.com/etc/designs/atvi/amazing-spiderman-game/amazing-spiderman-game-2/images/features/img5.jpg");
                        }
                        if (object.getAVFile("shopView") != null) {
                            shop.setShopBgUrl(object.getAVFile("shopView").getUrl());
                        } else {
                            shop.setShopBgUrl("http://2.media.dorkly.cvcdn.com/74/26/52efcad899df396d82793898513c86e7-its-official-spider-man-is-joining-the-marvel-cinematic-universe.jpg");
                        }
                        AVGeoPoint geoPoint = object.getAVGeoPoint("shopGeoPoint");
                        shop.setLat(geoPoint.getLatitude());
                        shop.setLon(geoPoint.getLongitude());
                        LatLng latLng = new LatLng(geoPoint.getLatitude(), geoPoint.getLongitude());
                        shop.setDistance(SphericalUtil.computeDistanceBetween(latLng, currentLocation) / 1000);
                        shop.setHasLike(true);
                        mlistCoffeeShop.add(shop);
                    }
                    // Add to ShopContainer for MapActivity
                    ShopContainer.getInstance().setShops(mlistCoffeeShop);

                    if (myRecyclerView.getAdapter() == null) {
                        myRecyclerView.setAdapter(mAdapter);
                    } else {
                        mAdapter.notifyDataSetChanged();
                    }
                    CollectionsView_Copy.this.setRefreshing(false);
                } else {
                    Log.d("失败", "查询错误: " + e.getMessage());
                    CollectionsView_Copy.this.setRefreshing(false);
                }
            }
        });

    }

    public void refreshContent() {
        hasMoreData = true;
        requestCollectionsShop(true);
    }

}
