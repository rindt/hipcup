package com.lesmtech.hipcup.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.avos.avoscloud.AVCloud;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.CountCallback;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.FunctionCallback;
import com.avos.avoscloud.GetCallback;
import com.google.android.gms.common.api.BatchResultToken;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
import com.lesmtech.hipcup.DetailActivity;
import com.lesmtech.hipcup.HomeActivity;
import com.lesmtech.hipcup.R;
import com.lesmtech.hipcup.entity.FilterSortModel;
import com.lesmtech.hipcup.entity.Shop;
import com.lesmtech.hipcup.json.Shops;
import com.lesmtech.hipcup.fragment.detail.ShopDetailFragment;
import com.lesmtech.hipcup.tool.HGson;
import com.lesmtech.hipcup.tool.ShopContainer;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;

/**
 * @author Rindt
 * @version 0.1
 * @since 6/25/15
 */
public class ShopsView extends SwipeRefreshLayout {

    private Context mContext;

    @InjectView(R.id.lv_shops)
    ListView lv_shops;

    private List<Shop> mlistCoffeeShop;

    private LatLng currentLocation;

    private boolean hasMoreData;

    private AlphaInAnimationAdapter mAlphaInAnimationAdapter;

    public ShopsView(Context context) {
        super(context);
        mContext = context;
        initView();
    }

    public void initView() {
        addView(LayoutInflater.from(mContext).inflate(R.layout.view_shop, null));
        ButterKnife.inject(this);
        mlistCoffeeShop = new ArrayList<>();
        mAlphaInAnimationAdapter = new AlphaInAnimationAdapter(shopsAdapter);
        mAlphaInAnimationAdapter.setAbsListView(lv_shops);
        setColorSchemeResources(R.color.blue300, R.color.blue500, R.color.blue700);
        this.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });
        // Show fresh icon when first loading.
        requestCoffeeShops(false);
    }

    public void requestCoffeeShops(Boolean refresh) {

        if(!refresh){
            ShopsView.this.setRefreshing(true);
        }

        currentLocation = ((HomeActivity) mContext).getCurrentLocation();

        AVQuery<AVObject> query = new AVQuery<>("CoffeeShop");

        // 实现分页, 一页20个
        if (!refresh) {
            query.setSkip(mlistCoffeeShop.size());
        } else {
            // Can do better, coz the view will be blank when loading
            mlistCoffeeShop.clear();
        }

        query.setLimit(20);
        query.findInBackground(new FindCallback<AVObject>() {

            public void done(List<AVObject> avObjects, AVException e) {
                if (e == null) {

                    // Whether has more data
                    hasMoreData = avObjects.size() == 20;

                    AVRelation<AVObject> relation = AVUser.getCurrentUser().getRelation("shopLike");

                    for (AVObject object : avObjects) {
                        final Shop shop = HGson.getInstance().fromJson(object.toJSONObject().toString(), Shop.class);
                        // Get id from the object identified the line of data
                        shop.setId(object.getObjectId());
                        if (object.getAVFile("shopLogo") != null) {
                            shop.setShopLogoUrl(object.getAVFile("shopLogo").getUrl());
                        } else {
                            shop.setShopLogoUrl("http://www.theamazingspidermangame.com/etc/designs/atvi/amazing-spiderman-game/amazing-spiderman-game-2/images/features/img5.jpg");
                        }
                        if (object.getAVFile("shopView") != null) {
                            shop.setShopBgUrl(object.getAVFile("shopView").getUrl());
                        } else {
                            shop.setShopBgUrl("http://2.media.dorkly.cvcdn.com/74/26/52efcad899df396d82793898513c86e7-its-official-spider-man-is-joining-the-marvel-cinematic-universe.jpg");
                        }
                        AVGeoPoint geoPoint = object.getAVGeoPoint("shopGeoPoint");
                        shop.setLat(geoPoint.getLatitude());
                        shop.setLon(geoPoint.getLongitude());
                        LatLng latLng = new LatLng(geoPoint.getLatitude(), geoPoint.getLongitude());
                        shop.setDistance(SphericalUtil.computeDistanceBetween(latLng, currentLocation) / 1000);

                        // Get whether the shop has been liked by the user
                        AVQuery query = relation.getQuery();
                        query.whereEqualTo("objectId", object.getObjectId());

                        query.countInBackground(new CountCallback() {
                            @Override
                            public void done(int i, AVException e) {
                                if (e == null) {
                                    if (i == 1) {
                                        shop.setHasLike(true);
                                    } else {
                                        shop.setHasLike(false);
                                    }
                                    mlistCoffeeShop.add(shop);
                                    // Add to ShopContainer for MapActivity
                                    ShopContainer.getInstance().setShops(mlistCoffeeShop);

                                    if (lv_shops.getAdapter() == null) {
                                        lv_shops.setAdapter(mAlphaInAnimationAdapter);
                                    } else {
                                        shopsAdapter.notifyDataSetChanged();
                                    }
                                    ShopsView.this.setRefreshing(false);
                                } else {
                                    System.out.println(e.getMessage());
                                }
                            }
                        });
                    }
                    Log.d("成功", "查询到" + avObjects.toString() + " 条符合条件的数据");
                } else {
                    Log.d("失败", "查询错误: " + e.getMessage());
                    ShopsView.this.setRefreshing(false);
                }
            }
        });

    }

    public BaseAdapter shopsAdapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return mlistCoffeeShop.size();
        }

        @Override
        public Object getItem(int position) {
            return mlistCoffeeShop.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = new ShopListItemView(mContext, (Shop) getItem(position));
            } else {
                ((ShopListItemView) convertView).setCoffeeShop((Shop) getItem(position));
            }
            if (mlistCoffeeShop.size() - position < 5 && hasMoreData) {
                requestCoffeeShops(false);
            }
            // Load more data
            return convertView;
        }
    };

    @OnItemClick(R.id.lv_shops)
    void onItemClickGoods(int position) {
        //  send intent with good id and fragment index
        Intent intent = new Intent(mContext, DetailActivity.class);
        intent.putExtra(ShopDetailFragment.SHOPID, mlistCoffeeShop.get(position).getId());
        intent.putExtra(DetailActivity.SOURCE, DetailActivity.SHOPDETAILFRAGMENT);
        mContext.startActivity(intent);
        ((Activity) mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.scale_out);
    }

    /**
     * updatesShops From Dialog that created in HomeActivity
     *
     * @param model
     */
    public void updateShops(FilterSortModel model) {

        FilterSortModel result = model;

        AVQuery<AVObject> query = new AVQuery<>("CoffeeShop");
        // 实现分页, 一页20个

//        query.setSkip(mlistCoffeeShop.size());
        query.setLimit(20);

        // Get current location from home activity
        LatLng lat = ((HomeActivity) mContext).getCurrentLocation();
        AVGeoPoint userLocation = new AVGeoPoint(lat.latitude, lat.longitude);

        if (result.getSortby() == 0) {
            query.whereNear("shopGeoPoint", userLocation);
        } else if (result.getSortby() == 1) {
            // Rating
            query.orderByDescending("rating");
        }

        // Filter distance
        int distance = result.getDistance();

        switch (distance) {
            case 0:
                query.whereWithinKilometers("shopGeoPoint", userLocation, 0.5);
                break;
            case 1:
                query.whereWithinKilometers("shopGeoPoint", userLocation, 2);
                break;
            case 2:
                query.whereWithinKilometers("shopGeoPoint", userLocation, 10);
                break;
            case 3:
                query.whereWithinKilometers("shopGeoPoint", userLocation, 25);
                break;
            case 4:
                query.whereWithinKilometers("shopGeoPoint", userLocation, 50);
                break;
            default:
                break;
        }

        // Filter Price
        int level = result.getPrice();

        switch (level) {
            case 0:
                query.whereEqualTo("priceLevel", 0);
                break;
            case 1:
                query.whereEqualTo("priceLevel", 1);
                break;
            case 2:
                query.whereEqualTo("priceLevel", 2);
                break;
            default:
                break;
        }

        // Wifi
        boolean hasWifi = result.isWifi();

        if (hasWifi) {
            query.whereEqualTo("hasWifi", true);
        }

        // Hour Limit
        boolean hasHourLimit = result.isHourLimit();
        if (hasHourLimit) {
            query.whereEqualTo("hourFree", true);
        }

        // Start search
        query.findInBackground(new FindCallback<AVObject>() {

            @Override
            public void done(List<AVObject> list, AVException e) {
                if (e == null) {

                    ShopsView.this.setRefreshing(true);

                    // Whether has more data

                    hasMoreData = (list.size() == 20);

                    if (list.size() == 0) {
                        ShopsView.this.setRefreshing(false);
                        Toast.makeText(mContext, "No Restaurant Matches. You are so mean.", Toast.LENGTH_SHORT).show();
                    } else {

                        mlistCoffeeShop.clear();
                        AVRelation<AVObject> relation = AVUser.getCurrentUser().getRelation("shopLike");

                        for (AVObject object : list) {

                            final Shop shop = HGson.getInstance().fromJson(object.toJSONObject().toString(), Shop.class);
                            // Get id from the object identified the line of data
                            shop.setId(object.getObjectId());
                            if (object.getAVFile("shopLogo") != null) {
                                shop.setShopLogoUrl(object.getAVFile("shopLogo").getUrl());
                            } else {
                                shop.setShopLogoUrl("http://www.theamazingspidermangame.com/etc/designs/atvi/amazing-spiderman-game/amazing-spiderman-game-2/images/features/img5.jpg");
                            }
                            if (object.getAVFile("shopView") != null) {
                                shop.setShopBgUrl(object.getAVFile("shopView").getUrl());
                            } else {
                                shop.setShopBgUrl("http://2.media.dorkly.cvcdn.com/74/26/52efcad899df396d82793898513c86e7-its-official-spider-man-is-joining-the-marvel-cinematic-universe.jpg");
                            }
                            AVGeoPoint geoPoint = object.getAVGeoPoint("shopGeoPoint");
                            shop.setLat(geoPoint.getLatitude());
                            shop.setLon(geoPoint.getLongitude());
                            LatLng latLng = new LatLng(geoPoint.getLatitude(), geoPoint.getLongitude());
                            shop.setDistance(SphericalUtil.computeDistanceBetween(latLng, currentLocation) / 1000);

                            //Test
                            System.out.println("Rating:" + shop.getRating());
                            System.out.println("Distance:" + shop.getDistance());
                            System.out.println("PriceLevel:" + shop.getPriceLevel());
                            System.out.println("Wifi:" + shop.isHasWifi());
                            System.out.println("HourFree" + shop.isHourFree());

                            // Get whether the shop has been liked by the user
                            AVQuery query = relation.getQuery();
                            query.whereEqualTo("objectId", object.getObjectId());

                            mlistCoffeeShop.add(shop);
                            // Add to ShopContainer for MapActivity
                            ShopContainer.getInstance().setShops(mlistCoffeeShop);

                            query.countInBackground(new CountCallback() {
                                @Override
                                public void done(int i, AVException e) {
                                    if (e == null) {
                                        if (i == 1) {
                                            shop.setHasLike(true);
                                        } else {
                                            shop.setHasLike(false);
                                        }
                                        if (lv_shops.getAdapter() == null) {
                                            lv_shops.setAdapter(mAlphaInAnimationAdapter);
                                        } else {
                                            shopsAdapter.notifyDataSetChanged();
                                            ShopsView.this.setRefreshing(false);
                                        }
                                    } else {
                                        System.out.println(e.getMessage());
                                    }
                                }
                            });
                        }
                    }
                } else {
                    System.out.println(e.getMessage());
                }
            }
        });

    }

    /**
     * The following codes are doing sort stuff in different way
     */

    // Sort by Rating
    private static int listSize = 0;

    public static void testCloudCode(double lat, double lon) {
        Map<String, Object> params = new HashMap<>();
        params.put("listSize", listSize);
        params.put("lat", lat);
        params.put("lon", lon);
        AVCloud.callFunctionInBackground("shopOrderByRating", params, new FunctionCallback<Object>() {
            public void done(Object object, AVException e) {
                if (e == null) {
                    System.out.println(object.toString());
                    Shops shops = HGson.getInstance().fromJson(object.toString(), Shops.class);
                    System.out.println(shops.getShop().get(0).getDistance());
                } else {
                    System.out.println("Error:" + e.getMessage());
                }
            }
        });
    }

    public void refreshContent() {
        hasMoreData = true;
        requestCoffeeShops(true);
    }
}
