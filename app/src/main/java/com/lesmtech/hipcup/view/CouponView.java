package com.lesmtech.hipcup.view;

import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.lesmtech.hipcup.R;
import com.lesmtech.hipcup.entity.Good;
import com.lesmtech.hipcup.widget.RoundedTransformation;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * This view is used to show coupon.
 *
 * @author Rindt
 * @version 0.1
 * @since 7/11/15
 */
public class CouponView extends FrameLayout {

    private Context mContext;

    private Good mGood;

    @InjectView(R.id.get_coupon)
    ImageButton getCoupon;

    @InjectView(R.id.image)
    ImageView couponImage;

    @InjectView(R.id.name)
    TextView couponName;

    @InjectView(R.id.fPrice)
    TextView fPrice;

    @InjectView(R.id.rPrice)
    TextView rPrice;

    public CouponView(Context context) {
        super(context);
        mContext = context;
        initView(context);
    }

    public CouponView(Context context, Good good) {
        super(context);
        mGood = good;
        mContext = context;
        initView(context);
    }

    public CouponView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView(context);
    }

    public CouponView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView(context);
    }

    public CouponView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
        initView(context);
    }

    private void initView(Context context) {
        addView(LayoutInflater.from(context).inflate(R.layout.view_coupon, null));
        ButterKnife.inject(this);
        if(mGood != null){
            setGood(mGood);
        }
    }

    public void setGood(Good good){
        couponName.setText(good.getGoodName());
        fPrice.setText(good.getfPrice());
        rPrice.setText(good.getrPrice());
        rPrice.setPaintFlags(rPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        Picasso.with(mContext).load(good.getGoodUrl()).transform(new RoundedTransformation(10, 0)).into(couponImage);
    }

    @OnClick(R.id.get_coupon)
    public void getCoupon(){

    }
}
