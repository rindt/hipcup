package com.lesmtech.hipcup.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.lesmtech.hipcup.R;
import com.lesmtech.hipcup.entity.Shop;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * @author Rindt
 * @version 0.1
 * @since 6/15/15
 */
public class ShopListItemView extends FrameLayout {

    private Shop mCoffeeShop;

    private Context mContext;

    private CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                System.out.println("checked");
                // Add shop to collections in user table
                AVQuery<AVObject> postQuery = new AVQuery<>("CoffeeShop");
                postQuery.getInBackground(mCoffeeShop.getId(), new GetCallback<AVObject>() {
                    @Override
                    public void done(AVObject avObject, AVException e) {
                        if (e == null) {
                            AVUser user = AVUser.getCurrentUser();
                            AVRelation<AVObject> relation = user.getRelation("shopLike");
                            relation.add(avObject);
                            user.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(AVException e) {
                                    if (e == null) {
                                        System.out.println("success");
                                        mCoffeeShop.setHasLike(true);
                                    } else {
                                        System.out.println(e.getMessage());
                                    }
                                }
                            });
                        } else {
                            System.out.println(e.getMessage());
                        }
                    }
                });

            } else {
                System.out.println("unchecked");
                // Add shop to collections in user table
                AVQuery<AVObject> postQuery = new AVQuery<>("CoffeeShop");
                postQuery.getInBackground(mCoffeeShop.getId(), new GetCallback<AVObject>() {
                    @Override
                    public void done(AVObject avObject, AVException e) {
                        if (e == null) {
                            AVUser user = AVUser.getCurrentUser();
                            AVRelation<AVObject> relation = user.getRelation("shopLike");
                            relation.remove(avObject);
                            user.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(AVException e) {
                                    if (e == null) {
                                        System.out.println("success");
                                        mCoffeeShop.setHasLike(false);
                                    } else {
                                        System.out.println(e.getMessage());
                                    }
                                }
                            });
                        } else {
                            System.out.println(e.getMessage());
                        }
                    }
                });
            }
        }
    };

    /**
     * This is used to set background, shopBgUrl;
     */
    @InjectView(R.id.iv_background)
    ImageView iv_background;

    /**
     * This is used to add shops to favourite;
     */
    @InjectView(R.id.number_of_good)
    TextView numberOfGood;

    @InjectView(R.id.tv_distance)
    TextView tv_distance;

    @InjectView(R.id.btn_like)
    ToggleButton btnLike;

    /**
     * This is to show shop name;
     */
    @InjectView(R.id.tv_name)
    TextView tv_name;

    /**
     * This is to show shop address;
     */
    @InjectView(R.id.tv_address)
    TextView tv_address;

    public ShopListItemView(Context context) {
        super(context);
        mContext = context;
        initView(context);
    }

    public ShopListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView(context);
    }

    public ShopListItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView(context);
    }

    public ShopListItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
    }

    public ShopListItemView(Context context, Shop coffeeShop) {
        super(context);
        mCoffeeShop = coffeeShop;
        mContext = context;
        initView(context);
    }


    private void initView(Context context) {
        addView(LayoutInflater.from(context).inflate(R.layout.list_item_coffee_shop, null));
        ButterKnife.inject(this);
        if (mCoffeeShop != null) {
            setCoffeeShop(mCoffeeShop);
        }
    }

    public void setCoffeeShop(Shop shop) {

        mCoffeeShop = shop;

        btnLike.setOnCheckedChangeListener(null);

        tv_name.setText(shop.getShopName());
        tv_address.setText(shop.getShopAddress());
        double distanceNumber = shop.getDistance();

        if (distanceNumber > 100) {
            tv_distance.setText("大于 100 km");
        } else {
            String result = String.format("%.1f", distanceNumber);
            tv_distance.setText(result + " km");
        }

        // Number of coupon
        numberOfGood.setText(String.valueOf(shop.getNumberOfGood()));
        // btn_like.setText(String.valueOf((int)shop.getNumberOfLike()));
        btnLike.setChecked(shop.isHasLike());
        Picasso.with(mContext).load(shop.getShopBgUrl()).into(iv_background);
        // After setting checked, set onCheckedChangeListener
        btnLike.setOnCheckedChangeListener(onCheckedChangeListener);
    }

}
