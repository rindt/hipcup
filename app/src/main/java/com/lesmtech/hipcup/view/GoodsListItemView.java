package com.lesmtech.hipcup.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.lesmtech.hipcup.R;
import com.lesmtech.hipcup.entity.Good;
import com.lesmtech.hipcup.entity.Shop;
import com.lesmtech.hipcup.widget.RoundedTransformation;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * @author Rindt
 * @version 0.1
 * @since 6/15/15
 */
public class GoodsListItemView extends FrameLayout {

    private Good mGood;

    private Context mContext;

    @InjectView(R.id.image)
    ImageView mGoodImage;

    /**
     * This is to show good name;
     */
    @InjectView(R.id.good_name)
    TextView goodName;

    @InjectView(R.id.restaurant_name)
    TextView restaurantName;

    @InjectView(R.id.distance)
    TextView distance;

    @InjectView(R.id.like)
    TextView numberOfLike;

    @InjectView(R.id.good_price)
    TextView goodPrice;

    public GoodsListItemView(Context context) {
        super(context);
        mContext = context;
        initView(context);
    }

    public GoodsListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView(context);
    }

    public GoodsListItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView(context);
    }

    public GoodsListItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
    }

    public GoodsListItemView(Context context, Good good) {
        super(context);
        mGood = good;
        mContext = context;
        initView(context);
    }


    private void initView(Context context) {
        addView(LayoutInflater.from(context).inflate(R.layout.list_item_good, null));
        ButterKnife.inject(this);
        if (mGood != null) {
            setGood(mGood);
        }
    }

    public void setGood(Good good) {
        goodName.setText(good.getGoodName());

        // number of like
        numberOfLike.setText(String.valueOf(good.getNumberOfLike()));

        // final price
        // Concern: good also has a regular price
        goodPrice.setText(good.getfPrice());

        // get shop from good;
        Shop shop = good.getShop();

        // show shop name from shop of good
        restaurantName.setText(shop.getShopName());

        // distance
        double distanceNumber = shop.getDistance();
        if (distanceNumber > 100) {
            distance.setText(">100km");
        } else {
            distance.setText(distanceNumber + "km");
        }
        Picasso.with(mContext).load(good.getGoodUrl()).transform(new RoundedTransformation(50,5)).into(mGoodImage);
    }
}
