package com.lesmtech.hipcup.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.AndroidCharacter;
import android.text.method.CharacterPickerDialog;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Space;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
import com.lesmtech.hipcup.DetailActivity;
import com.lesmtech.hipcup.HomeActivity;
import com.lesmtech.hipcup.R;
import com.lesmtech.hipcup.entity.Good;
import com.lesmtech.hipcup.entity.Shop;
import com.lesmtech.hipcup.fragment.detail.GoodDetailFragment;
import com.lesmtech.hipcup.tool.HGson;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;

/**
 * @author Rindt
 * @version 0.1
 * @since 6/25/15
 */
public class GoodsView extends FrameLayout {

    private Context mContext;

    @InjectView(R.id.lv_goods)
    ListView lvGoods;

    private List<Good> mListGood;

    private LatLng currentLocation;

    private boolean hasMoreData;

    private AlphaInAnimationAdapter mAlphaInAnimationAdapter;

    public GoodsView(Context context) {
        super(context);
        mContext = context;
        initView();
    }

    public GoodsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView();
    }

    public GoodsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView();
    }

    public GoodsView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
        initView();
    }

    public void initView() {
        addView(LayoutInflater.from(mContext).inflate(R.layout.view_good, null));
        ButterKnife.inject(this);
        mListGood = new ArrayList<>();
        mAlphaInAnimationAdapter = new AlphaInAnimationAdapter(goodsAdapter);
        mAlphaInAnimationAdapter.setAbsListView(lvGoods);
        requestCoffeeShops();
    }

    public void requestCoffeeShops() {

        currentLocation = ((HomeActivity) mContext).getCurrentLocation();

        AVQuery<AVObject> query = new AVQuery<>("Goods");
        query.setLimit(20);
        query.findInBackground(new FindCallback<AVObject>() {
            public void done(final List<AVObject> avObjects, AVException e) {

                if (e == null) {

                    final int numberOfNewData;

                    if (mListGood.size() == 0) {
                        numberOfNewData = avObjects.size();
                    } else {
                        numberOfNewData = mListGood.size() + avObjects.size();
                    }

                    for (AVObject object : avObjects) {
                        final Good good = HGson.getInstance().fromJson(object.toJSONObject().toString(), Good.class);
                        good.setGoodUrl(object.getAVFile("pic").getUrl());
                        AVQuery<AVObject> shop = AVRelation.reverseQuery("CoffeeShop", "goods", object);
                        shop.getFirstInBackground(new GetCallback<AVObject>() {
                            @Override
                            public void done(AVObject object, AVException e) {
                                if (e == null) {
                                    // Set all data that related to goods like geo point, restaurant
                                    // Also calculate the distance based on geoPoint
                                    // One good can only be existed to one Shop
                                    System.out.println("get here");
                                    Shop shop = HGson.getInstance().fromJson(object.toJSONObject().toString(), Shop.class);
                                    shop.setShopLogoUrl(object.getAVFile("shopLogo").getUrl());
                                    shop.setShopBgUrl(object.getAVFile("shopView").getUrl());
                                    AVGeoPoint geoPoint = object.getAVGeoPoint("shopGeoPoint");
                                    LatLng latLng = new LatLng(geoPoint.getLatitude(), geoPoint.getLongitude());
                                    shop.setDistance(SphericalUtil.computeDistanceBetween(latLng, currentLocation));
                                    good.setShop(shop);
                                    mListGood.add(good);
                                    if (numberOfNewData == mListGood.size()) {
                                        if (lvGoods.getAdapter() == null) {
                                            lvGoods.setAdapter(mAlphaInAnimationAdapter);
                                        } else {
                                            goodsAdapter.notifyDataSetChanged();
                                        }
                                    }
                                } else {
                                    Log.d("Error", e.getMessage());
                                }
                            }
                        });
                    }
                    Log.d("成功", "查询到" + avObjects.toString() + " 条符合条件的数据");

                } else {
                    Log.d("失败", "查询错误: " + e.getMessage());
                }
            }
        });

    }

    public BaseAdapter goodsAdapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return mListGood.size();
        }

        @Override
        public Object getItem(int position) {
            return mListGood.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = new GoodsListItemView(mContext, (Good) getItem(position));
            } else {
                ((GoodsListItemView) convertView).setGood((Good) getItem(position));
            }

            // Load more data
            return convertView;
        }
    };

    @OnItemClick(R.id.lv_goods)
    void onItemClickGoods(int position) {
        //  send intent with good id and fragment index
        Intent intent = new Intent(mContext, DetailActivity.class);
        intent.putExtra(GoodDetailFragment.GOODID, mListGood.get(position).getGoodId());
        intent.putExtra(DetailActivity.SOURCE, DetailActivity.GOODDETAILFRAGMENT);
        mContext.startActivity(intent);
        ((Activity) mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.scale_out);
    }

}
