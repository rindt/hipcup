package com.lesmtech.hipcup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVCloud;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.FunctionCallback;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.lesmtech.hipcup.entity.FilterSortModel;
import com.lesmtech.hipcup.entity.Session;
import com.lesmtech.hipcup.tool.SlidingTabLayout;
import com.lesmtech.hipcup.view.CollectionsView;
import com.lesmtech.hipcup.view.CollectionsView_Copy;
import com.lesmtech.hipcup.view.DialogFilterView;
import com.lesmtech.hipcup.view.GoodsView;
import com.lesmtech.hipcup.view.GoodsView_Copy;
import com.lesmtech.hipcup.view.ShopsView;
import com.squareup.okhttp.Call;

import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


/**
 * Main activity includes CoffeeShopFragment (MainFragment), CoffeeDetailFragment, CoffeeCheckFragment, CoffeeShopMapFragment
 *
 * @author Rindt
 * @version 0.1
 * @since 6/14/15
 */
public class HomeActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private CallbackManager callbackManager;

    double latmock = 38.8759144;
    double lonmock = -77.3600358;

    @InjectView(R.id.toolbar)
    Toolbar mToolbar;

    @InjectView(R.id.sliding_tabs)
    SlidingTabLayout slidingTabs;

    @InjectView(R.id.viewpager)
    ViewPager mViewPager;

    // bottom function button (TextView)
    @InjectView(R.id.maps)
    TextView maps;

    // Navigation Drawer
    @InjectView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @InjectView(R.id.right_drawer_container)
    LinearLayout mDrawerContainer;

    private String[] mPlanetTitles;

    private final String TAG_SHOPSVIEW = "shopsView";

    private final String TAG_GOODSVIEW = "goodsView";

    private final String TAG_COLLECTION = "collection";

    @InjectView(R.id.right_drawer)
    ListView mDrawerList;

    private SamplePagerAdapter mSamplePagerAdapter;

    /**
     * Google Map Widget
     */
    private GoogleApiClient mGoogleApiClient;

    private Location mLastLocation;

    private Location mCurrentLocation;

    private double lat = 34.11111;

    private double lon = 35.11111;

    private LocationRequest mLocationRequest;

    private boolean mRequestingLocationUpdates = true;

    private String mLastUpdateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.inject(this);

        // Change Status Bar Color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.black));
        }

        // Facebook login widget, this is for user to share content to facebook
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        System.out.println("Facebook Success");
                        // Sync data to LeanCloud
                        // Not Necessary, if we need in the future, we can add
                        Toast.makeText(HomeActivity.this, "Connected with your facebook...", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(HomeActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });

        setSupportActionBar(mToolbar);
        setTitle("");

        // Set nevigation drawer, test
        mPlanetTitles = getResources().getStringArray(R.array.drawer_title);
        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<>(this,
                R.layout.drawer_list_item, mPlanetTitles));

        // Set the list's click listener

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

//        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
//                mToolbar, R.string.drawer_open, R.string.drawer_close) {
//
//            /** Called when a drawer has settled in a completely closed state. */
//            public void onDrawerClosed(View view) {
//                super.onDrawerClosed(view);
//            }
//
//            /** Called when a drawer has settled in a completely open state. */
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
//            }
//        };

//        // Set the drawer toggle as the DrawerListener
//        mSamplePagerAdapter = new SamplePagerAdapter();
//        mViewPager.setAdapter(mSamplePagerAdapter);
//
//        slidingTabs.setViewPager(mViewPager);

        buildGoogleApiClient();

        createLocationRequest();

    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    /**
     * Swaps fragments in the main content view
     */
    private void selectItem(int position) {
        // Create a new fragment and specify the planet to show based on position
        switch (position) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                clearSession();
                break;
        }
        // Highlight the selected item, update the title, and close the drawer
        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawers();
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
//        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
//        if (mDrawerToggle.onOptionsItemSelected(item)) {
//            return true;
//        }
        // Handle your other action bar items...
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.profile:
                // Show Navigation Bar
                mDrawerLayout.openDrawer(mDrawerContainer);
                break;
        }
        return true;
    }

    @OnClick(R.id.maps)
    public void btn_maps() {
        // Show maps
        Intent intent = new Intent(this, MapActivity.class);
        // Case 1 and 2 both have multiple values, set ShopContainers before start intent
        switch (mViewPager.getCurrentItem()) {
            case 0:
                // Shops View
                intent.putExtra(MapActivity.SOURCE, MapActivity.MULTIPLE);
                break;
            case 1:
                intent.putExtra(MapActivity.SOURCE, MapActivity.MULTIPLE);
                break;
            default:
                break;
        }
        startActivity(intent);
    }

    @OnClick(R.id.collection)
    public void btn_collection() {
        Intent intent = new Intent(this, CollectionActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @OnClick(R.id.coupon)
    public void btn_coupon() {
        Intent intent = new Intent(this, CouponActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    /**
     * All below are Google Map Staff
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    /**
     * Google Map Interface Method
     *
     * @param bundle
     */
    @Override
    public void onConnected(Bundle bundle) {

        Log.d("Location", "SUCCESS");

        // Key line to get last location
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if (mLastLocation != null) {
            Log.d("Location", String.valueOf(mLastLocation.getLatitude()));
            Log.d("Location", String.valueOf(mLastLocation.getLongitude()));
            lat = mLastLocation.getLatitude();
            lon = mLastLocation.getLongitude();
        }

        // Update location
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }

        // Set the drawer toggle as the DrawerListener
        mSamplePagerAdapter = new SamplePagerAdapter();
        mViewPager.setAdapter(mSamplePagerAdapter);
        slidingTabs.setViewPager(mViewPager);
    }

    /**
     * Google Map Interface Method
     *
     * @param i
     */
    @Override
    public void onConnectionSuspended(int i) {
        Log.d("Connect", "Suspended");
    }

    /**
     * Google Map Interface Method
     *
     * @param connectionResult
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d("Connect", "FAIL");
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        updateUI();
    }

    private void updateUI() {
        Log.d("CurrentLat", String.valueOf(mCurrentLocation.getLatitude()));
        Log.d("CurrentLon", String.valueOf(mCurrentLocation.getLongitude()));
        lat = mCurrentLocation.getLatitude();
        lon = mCurrentLocation.getLongitude();
        Log.d("updateTime", mLastUpdateTime);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    /**
     * The {@link android.support.v4.view.PagerAdapter} used to display pages in this sample.
     * The individual pages are simple and just display two lines of text. The important section of
     * this class is the {@link #getPageTitle(int)} method which controls what is displayed in the
     * {@link SlidingTabLayout}.
     */
    /**
     * The {@link android.support.v4.view.PagerAdapter} used to display pages in this sample.
     * The individual pages are simple and just display two lines of text. The important section of
     * this class is the {@link #getPageTitle(int)} method which controls what is displayed in the
     * {@link SlidingTabLayout}.
     */
    class SamplePagerAdapter extends PagerAdapter {

        /**
         * @return the number of pages to display
         */
        @Override
        public int getCount() {
            return 3;
        }

        /**
         * @return true if the value returned from {@link #instantiateItem(ViewGroup, int)} is the
         * same object as the {@link View} added to the {@link ViewPager}.
         */
        @Override
        public boolean isViewFromObject(View view, Object o) {
            return o == view;
        }

        /**
         * Return the title of the item at {@code position}. This is important as what this method
         * returns is what is displayed in the {@link SlidingTabLayout}.
         * <p/>
         * Here we construct one using the position value, but for real application the title should
         * refer to the item's contents.
         */
        @Override
        public CharSequence getPageTitle(int position) {
            return "Item " + (position + 1);
        }

        /**
         * Instantiate the {@link View} which should be displayed at {@code position}. Here we
         * inflate a layout from the apps resources and then change the text view to signify the position.
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            // Inflate a new layout from our resources
            View view = null;

            switch (position) {
                case 0:
                    view = new ShopsView(HomeActivity.this);
                    view.setTag(TAG_SHOPSVIEW);
                    break;
                case 1:
                    view = new GoodsView_Copy(HomeActivity.this);
                    view.setTag(TAG_GOODSVIEW);
                    break;
                case 2:
                    view = new CollectionsView_Copy(HomeActivity.this);
                    view.setTag(TAG_COLLECTION);
                    break;
            }
            // Add the newly created View to the ViewPager
            container.addView(view);
            // Return the View
            return view;
        }

        /**
         * Destroy the item from the {@link ViewPager}. In our case this is simply removing the
         * {@link View}.
         */
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public LatLng getCurrentLocation() {
        return new LatLng(lat, lon);
    }


    @OnClick(R.id.filter)
    public void popFilter() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this, R.style.PauseDialog);
        DialogFilterView content = new DialogFilterView(this);
        alertBuilder.setView(content);
        AlertDialog dialog = alertBuilder.create();
        // Set dialog, so the view can toggle the dialog
        content.setDialog(dialog);
        dialog.show();
    }

    private void createMockData() {
        //Add mock data
        for (int i = 0; i < 20; i++) {
            AVObject object = new AVObject("CoffeeShop");
            object.put("shopOpenHour", "Mon-Fri:9:00 am - 8:00pm ; Sat-Sun: 9:00 am - 6:00 pm");
            object.put("shopGeoPoint", new AVGeoPoint(latmock, lonmock));
            object.put("shopAddress", "1600 Eads Ln");
            object.put("shopName", "Rindt Coffeeeee Shop");
            object.put("shopPhone", "6178006197");
            latmock += 0.02;
            lonmock += 0.02;
            object.saveInBackground();
        }
    }

    public void getSortResult(FilterSortModel model) {
        ShopsView view = (ShopsView) mViewPager.findViewWithTag(TAG_SHOPSVIEW);
        System.out.println("GetSortResult");
        view.updateShops(model);
    }

    /**
     * Logout: clear session, also clear facebook session
     */
    private void clearSession() {
        SharedPreferences shre = getSharedPreferences("session", MODE_PRIVATE);
        SharedPreferences.Editor editor = shre.edit();
        editor.clear();
        editor.apply();

        // Clear Facebook Session
        LoginManager.getInstance().logOut();

        Intent boardingIntent = new Intent(this, BoardingActivity.class);
        startActivity(boardingIntent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    // Call in the ShopCollectionsAdapter for each item, if the user haven't logged in the applicaiton with facebook
    public void connectWithFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email", "user_birthday"));
    }
}
