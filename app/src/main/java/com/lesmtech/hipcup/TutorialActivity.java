package com.lesmtech.hipcup;

import android.support.v7.app.AppCompatActivity;

/**
 * This activity is used to show tutorials to user.
 * Also the user can regain the activity through setting (How it works.)
 * @author Rindt
 * @version 0.1
 * @since 6/14/15
 */
public class TutorialActivity extends AppCompatActivity {
}
