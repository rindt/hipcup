package com.lesmtech.hipcup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.lesmtech.hipcup.fragment.detail.GoodDetailFragment;
import com.lesmtech.hipcup.fragment.detail.ShopDetailFragment;

/**
 * This activity is used to show coupou detail or restaurant detail
 * Two fragment inside the activity
 *
 * @author Rindt
 * @version 0.1
 * @since 6/28/15
 */
public class DetailActivity extends AppCompatActivity {

    /**
     * 1, good_detail
     * 2, shop_detail
     */
    public final static String SOURCE = "source";

    public final static String GOODDETAILFRAGMENT = "good_detail";

    public final static String SHOPDETAILFRAGMENT = "shop_detail";

    // value for good detail, get from good view
    private long goodID;

    // value for shop detail, get from shop view
    private String shopID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        // Due to the different source, inflate the activity with different fragment
        if (savedInstanceState == null) {
            Intent intent = getIntent();
            if (intent.getStringExtra(SOURCE).equals(GOODDETAILFRAGMENT)) {
                goodID = intent.getLongExtra(GoodDetailFragment.GOODID, -1);
                Fragment goodDetailFragment = new GoodDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString(GoodDetailFragment.GOODID, String.valueOf(goodID));
                goodDetailFragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.content, goodDetailFragment).commit();
            } else {
                shopID = intent.getStringExtra(ShopDetailFragment.SHOPID);
                Fragment shopDetailFragment = new ShopDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString(ShopDetailFragment.SHOPID, shopID);
                shopDetailFragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.content, shopDetailFragment).commit();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.scale_in, R.anim.slide_out_right);
    }
}
