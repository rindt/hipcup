package com.lesmtech.hipcup.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.facebook.login.widget.LoginButton;

/**
 * @author Rindt
 * @version 0.1
 * @since 7/5/15
 */
public class HipcupFacebookButton extends LoginButton {
    public HipcupFacebookButton(Context context) {
        super(context);
        init();
    }

    public HipcupFacebookButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HipcupFacebookButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        Drawable drawable = getResources().getDrawable(com.facebook.R.drawable.com_facebook_button_icon);
        setCompoundDrawables(drawable, null, null, null);
        setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        setContentDescription("LOG IN WITH FACEBOOK");
    }
}
