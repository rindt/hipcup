package com.lesmtech.hipcup.tool;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * This tool is used to request server with HTTP GET
 *
 * @author Rindt
 * @version 0.1
 * @since 6/14/15
 */
public final class HttpGetClient extends OkHttpClient {

    public static final MediaType JSON
            = MediaType.parse("application/json");

    private static HttpGetClient client;

    public static HttpGetClient getInstance() {
        if (client == null) {
            client = new HttpGetClient();
        }
        return client;
    }

    public String get(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

}
