package com.lesmtech.hipcup.tool;

import android.util.Log;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * This singleton used to post json to server
 *
 * @author Rindt
 * @version 0.1
 * @since 6/14/15
 */
public final class HttpPostClient extends OkHttpClient {

    public static final MediaType JSON
            = MediaType.parse("application/json");

    private static HttpPostClient client;

    public static HttpPostClient getInstance() {
        if (client == null) {
            client = new HttpPostClient();
        }
        return client;
    }

    public String post(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Log.d("url", url);
        Log.d("post_json", json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

}
