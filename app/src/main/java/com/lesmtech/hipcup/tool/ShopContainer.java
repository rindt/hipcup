package com.lesmtech.hipcup.tool;

import com.lesmtech.hipcup.entity.Shop;

import java.util.List;

/**
 * This class is to save the shops for map activity
 * @author Rindt
 * @version 0.1
 * @since 7/2/15
 */
public class ShopContainer {

    private static ShopContainer mContainer;

    private List<Shop> mShops;

    private ShopContainer(){

    }

    public static ShopContainer getInstance(){
        if(mContainer == null){
            mContainer = new ShopContainer();
        }
        return mContainer;
    }

    public void setShops(List<Shop> mShops){
        this.mShops = mShops;
    }

    public List<Shop> getShops(){
        if(mShops == null){
            return null;
        }
        return mShops;
    }

    public boolean getClear(){
        mShops = null;
        return true;
    }

 }
