package com.lesmtech.hipcup.tool;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * This singleton is used to parse json to class and verse.
 *
 * @author Rindt
 * @version 0.1
 * @since 6/14/15
 */
public final class HGson {

    private static Gson singleton;

    private HGson() {

    }

    public static Gson getInstance() {
        if (singleton == null) {
            final GsonBuilder mBuilder = new GsonBuilder();
            // Customize rules
//            mBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
            singleton = mBuilder.create();
            return singleton;
        }
        return singleton;
    }

}
