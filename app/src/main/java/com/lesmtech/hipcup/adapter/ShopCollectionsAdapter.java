package com.lesmtech.hipcup.adapter;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.lesmtech.hipcup.HomeActivity;
import com.lesmtech.hipcup.R;
import com.lesmtech.hipcup.entity.FacebookUser;
import com.lesmtech.hipcup.entity.Shop;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * @author Rindt
 * @version 0.1
 * @since 7/23/15
 */
public class ShopCollectionsAdapter extends RecyclerView.Adapter<ShopCollectionsAdapter.ViewHolder> {

    private List<Shop> mShops;

    private Context mContext;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        @InjectView(R.id.background)
        public ImageView background;

        @InjectView(R.id.share)
        public TextView share;

        @InjectView(R.id.btn_share)
        ImageButton btnShare;

        @InjectView(R.id.number_of_good)
        public TextView numberOfGood;

        @InjectView(R.id.tv_address)
        public TextView address;

        @InjectView(R.id.tv_name)
        public TextView name;

        @InjectView(R.id.number_of_Like)
        TextView numberOfLike;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.inject(this, v);
        }

        @Override
        public void onClick(View v) {
            System.out.println(getPosition());
        }
    }

    public ShopCollectionsAdapter(List<Shop> mShops, Context context) {
        this.mShops = mShops;
        this.mContext = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ShopCollectionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_collection, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your data set at this position
        // - replace the contents of the view with that element
        final Shop shop = mShops.get(position);
        holder.numberOfGood.setText(String.valueOf(shop.getNumberOfGood()));
        holder.numberOfLike.setText(String.valueOf(shop.getNumberOfLike()));
        holder.name.setText(shop.getShopName());
        holder.address.setText(shop.getShopAddress());
        Picasso.with(mContext).load(shop.getShopBgUrl()).into(holder.background);
        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLogInWithFacebook()) {
                   showShareDialog(shop);
                } else {
                    // Please connect with your facebook
                    System.out.println("False");
                    connectToYourFacebook();
                }
            }
        });

        holder.btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLogInWithFacebook()) {
                    showShareDialog(shop);
                } else {
                    // Please connect with your facebook
                    connectToYourFacebook();
                }
            }
        });

    }

    private boolean isLogInWithFacebook() {

        if (AccessToken.getCurrentAccessToken() != null) {
            return true;
        }
        return false;
    }

    private void showShareDialog(Shop shop){
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setImageUrl(Uri.parse(shop.getShopBgUrl()))
                .setContentTitle(shop.getShopName())
                .setContentDescription(shop.getDescription())
                .setContentUrl(Uri.parse("https://developers.facebook.com"))
                .build();
        ShareDialog.show((Activity)mContext, content);
    }

    private void connectToYourFacebook(){
        ((HomeActivity)mContext).connectWithFacebook();
    }

    @Override
    public int getItemCount() {
        return mShops.size();
    }

}
