package com.lesmtech.hipcup;

import android.support.v7.app.AppCompatActivity;

/**
 * This activity is used to show user's profile including Adding Credit Card Associated,
 * Credit balance, Personal Information like email and username
 *
 * @author Rindt
 * @version 0.1
 * @since 6/14/15
 */
public class UserProfileActivity extends AppCompatActivity {
}
