package com.lesmtech.hipcup;

import android.support.v7.app.AppCompatActivity;

/**
 * This activity is used to get feedback from users.
 * User can type feedback to us to improve anything we have to.
 * @author Rindt
 * @version 0.1
 * @since 6/14/15
 */
public class FeedbackActivity extends AppCompatActivity {
}
