package com.lesmtech.hipcup.json;

import com.lesmtech.hipcup.entity.Shop;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rindt
 * @version 0.1
 * @since 7/5/15
 */
public class Shops {

    List<Shop> shop = new ArrayList<>();

    public List<Shop> getShop() {
        return shop;
    }

    public void setShop(List<Shop> shop) {
        this.shop = shop;
    }
}
