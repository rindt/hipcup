package com.lesmtech.hipcup.entity;


import com.google.gson.annotations.SerializedName;

/**
 * @author Rindt
 * @version 0.1
 * @since 6/26/15
 */
public class Good {

    @SerializedName("objectId")
    private String goodId;

    @SerializedName("name")
    private String goodName;

    private String goodUrl;

    private String rPrice;

    private String fPrice;

    private String nTotal;

    // Concern: might change to int
    // 1 as Drink
    // 2 as Food?
    // 3 as Something else
    private String category;

    private int numberOfLike;

    private int numberOfLeft;

    private int numberOfTotal;

    private String description;

//    // The following variables are used to show the distance between location and
//    // It is controversial to be a value in the good class, but to provide business logic needs
//    private String distance;
//
//    private String shopName;

    private Shop shop;

    public Good(){

    }

    public Good(Shop shop, String distance, String goodId, String goodName, String goodUrl, String rPrice, String fPrice, String nTotal, String category, int numberOfLike, int numberOfLeft, int numberOfTotal, String description) {
        this.shop = shop;
        this.goodId = goodId;
        this.goodName = goodName;
        this.goodUrl = goodUrl;
        this.rPrice = rPrice;
        this.fPrice = fPrice;
        this.nTotal = nTotal;
        this.category = category;
        this.numberOfLike = numberOfLike;
        this.numberOfLeft = numberOfLeft;
        this.numberOfTotal = numberOfTotal;
        this.description = description;
    }

    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getGoodUrl() {
        return goodUrl;
    }

    public void setGoodUrl(String goodUrl) {
        this.goodUrl = goodUrl;
    }

    public String getrPrice() {
        return rPrice;
    }

    public void setrPrice(String rPrice) {
        this.rPrice = rPrice;
    }

    public String getfPrice() {
        return fPrice;
    }

    public void setfPrice(String fPrice) {
        this.fPrice = fPrice;
    }

    public String getnTotal() {
        return nTotal;
    }

    public void setnTotal(String nTotal) {
        this.nTotal = nTotal;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getNumberOfLike() {
        return numberOfLike;
    }

    public void setNumberOfLike(int numberOfLike) {
        this.numberOfLike = numberOfLike;
    }

    public int getNumberOfLeft() {
        return numberOfLeft;
    }

    public void setNumberOfLeft(int numberOfLeft) {
        this.numberOfLeft = numberOfLeft;
    }

    public int getNumberOfTotal() {
        return numberOfTotal;
    }

    public void setNumberOfTotal(int numberOfTotal) {
        this.numberOfTotal = numberOfTotal;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }
}
