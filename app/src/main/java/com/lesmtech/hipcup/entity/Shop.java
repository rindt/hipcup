package com.lesmtech.hipcup.entity;

/**
 * @author Rindt
 * @version 0.1
 * @since 6/15/15
 */
public class Shop {

    private String id;

    private String shopName;

    private String shopAddress;

    private String shopOpenHour;

    private String shopLogoUrl;

    private String shopBgUrl;

    private String shopPhone;

    private int numberOfLike;

    private double lat;

    private double lon;

    private double distance;

    private int numberOfGood;

    private int rating;

    private int priceLevel;

    private boolean hasLike;

    private boolean hourFree;

    private boolean hasWifi;

    private String description;

    public Shop() {

    }

    public boolean isHasLike() {
        return hasLike;
    }

    public void setHasLike(boolean hasLike) {
        this.hasLike = hasLike;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getShopOpenHour() {
        return shopOpenHour;
    }

    public void setShopOpenHour(String shopOpenHour) {
        this.shopOpenHour = shopOpenHour;
    }

    public String getShopLogoUrl() {
        return shopLogoUrl;
    }

    public void setShopLogoUrl(String shopLogoUrl) {
        this.shopLogoUrl = shopLogoUrl;
    }

    public String getShopBgUrl() {
        return shopBgUrl;
    }

    public void setShopBgUrl(String shopBgUrl) {
        this.shopBgUrl = shopBgUrl;
    }

    public String getShopPhone() {
        return shopPhone;
    }

    public void setShopPhone(String shopPhone) {
        this.shopPhone = shopPhone;
    }


    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Shop(String id, String description,int numberOfGood, boolean hourFree, boolean hasWifi, int priceLevel, int rating, boolean hasLike, int numberOfLike, String shopName, String shopAddress, String shopOpenHour, String shopLogoUrl, String shopBgUrl, String shopPhone, double lat, double lon, double distance) {
        this.id = id;
        this.numberOfLike = numberOfLike;
        this.hasLike = hasLike;
        this.hourFree = hourFree;
        this.hasWifi = hasWifi;
        this.priceLevel = priceLevel;
        this.rating = rating;
        this.numberOfGood = numberOfGood;
        this.shopName = shopName;
        this.shopAddress = shopAddress;
        this.shopOpenHour = shopOpenHour;
        this.shopLogoUrl = shopLogoUrl;
        this.shopBgUrl = shopBgUrl;
        this.shopPhone = shopPhone;
        this.lat = lat;
        this.description = description;
        this.lon = lon;
        this.distance = distance;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public int getNumberOfLike() {
        return numberOfLike;
    }

    public void setNumberOfLike(int numberOfLike) {
        this.numberOfLike = numberOfLike;
    }

    public int getNumberOfGood() {
        return numberOfGood;
    }

    public void setNumberOfGood(int numberOfGood) {
        this.numberOfGood = numberOfGood;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getPriceLevel() {
        return priceLevel;
    }

    public void setPriceLevel(int priceLevel) {
        this.priceLevel = priceLevel;
    }

    public boolean isHasWifi() {
        return hasWifi;
    }

    public void setHasWifi(boolean hasWifi) {
        this.hasWifi = hasWifi;
    }

    public boolean isHourFree() {
        return hourFree;
    }

    public void setHourFree(boolean hourFree) {
        this.hourFree = hourFree;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
