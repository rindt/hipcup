package com.lesmtech.hipcup.entity;

/**
 * @author Rindt
 * @version 0.1
 * @since 7/22/15
 */
public class Session {

    public static final String ID = "id";

    public static final String USERNAME = "username";

    public static final String EMAIL = "email";

    public static final String BIRTHDAY = "birthday";

    public static final String PHONENUMBER = "phonenumber";

    public static final String FULLNAME = "fullName";

    public static final String GENDER = "gender";

}
