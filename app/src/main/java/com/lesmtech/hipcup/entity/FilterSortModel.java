package com.lesmtech.hipcup.entity;

/**
 * This model is used to save FilterSort Selection, so the list in home activity can be updated based on this.
 *
 * @author Rindt
 * @version 0.1
 * @since 7/3/15
 */
public class FilterSortModel {

    /**
     * sortBy
     * Unselected : -1
     * Distance : 0
     * Rating : 1
     */
    private int sortby = -1;

    /**
     * Distance
     * Unselected : -1
     * < 0.5 : 0
     * < 2 : 1
     * < 10 : 2
     * < 25 : 3
     * < 50 : 4
     */
    private int distance = -1;

    /**
     * Price
     * Unselected : -1;
     * 0 <= 50
     * 1 <= 100
     * 2 >= 100
     */
    private int price = -1;

    /**
     * Minimalist, Industrial, Futuristic, Vintage, Western, Eastern, Exotic, Modern, Victorian
     */
    private int selectedInteriorDeisgn = -1;

    /**
     * Pop, Classical, Jazz, Rock, Beat, hip-hop
     */
    private int selectedMusic = -1;

    /**
     * Sea, Green, City, (Open)	Any
     */
    private int selectedView = -1;

    /**
     * Wifi
     * If false, not an option to sort
     */
    private boolean wifi = false;

    /**
     * Hour Limit
     * If false, the user doesn't care;
     */
    private boolean hourLimit = false;

    public FilterSortModel(int sortby, int distance, int price, boolean wifi, boolean hourLimit) {
        this.sortby = sortby;
        this.distance = distance;
        this.price = price;
        this.wifi = wifi;
        this.hourLimit = hourLimit;
    }

    public int getSortby() {
        return sortby;
    }

    public void setSortby(int sortby) {
        this.sortby = sortby;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isWifi() {
        return wifi;
    }

    public void setWifi(boolean wifi) {
        this.wifi = wifi;
    }

    public boolean isHourLimit() {
        return hourLimit;
    }

    public void setHourLimit(boolean hourLimit) {
        this.hourLimit = hourLimit;
    }
}
