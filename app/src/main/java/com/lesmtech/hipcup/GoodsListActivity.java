package com.lesmtech.hipcup;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.lesmtech.hipcup.entity.Good;
import com.lesmtech.hipcup.entity.Shop;
import com.lesmtech.hipcup.fragment.detail.ShopDetailFragment;
import com.lesmtech.hipcup.tool.HGson;
import com.lesmtech.hipcup.view.CouponView;
import com.lesmtech.hipcup.view.GoodsListItemView;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * This activity is used to show all coupons that the vennor haves, using water fall flow.
 *
 * @author Rindt
 * @version 0.1
 * @since 7/12/15
 */
public class GoodsListActivity extends AppCompatActivity {

    @InjectView(R.id.gv_goods)
    GridView gridView;

    private List<Good> mGoods;

    private String shopId;

    private AlphaInAnimationAdapter mAlphaInAnimationAdapter;

    private GoodsDetailAdapter mGoodsDetailAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goodsdetail);
        ButterKnife.inject(this);
        shopId = getIntent().getStringExtra(ShopDetailFragment.SHOPID);

        mGoods = new ArrayList<>();
        mGoodsDetailAdapter = new GoodsDetailAdapter();

        mAlphaInAnimationAdapter = new AlphaInAnimationAdapter(mGoodsDetailAdapter);
        mAlphaInAnimationAdapter.setAbsListView(gridView);

        // Request all goods from server
        if (shopId == null) {
            // do nothing
        } else {
            requestGoodsFromServer();
        }

    }

    public class GoodsDetailAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mGoods.size();
        }

        @Override
        public Object getItem(int position) {
            return mGoods.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = new CouponView(GoodsListActivity.this, mGoods.get(position));
            } else {
                ((CouponView) convertView).setGood(mGoods.get(position));
            }
            return convertView;
        }
    }

    private void requestGoodsFromServer() {
        // Get shop, so can get goods;
        AVQuery<AVObject> query = new AVQuery<>("CoffeeShop");
        query.getInBackground(shopId, new GetCallback<AVObject>() {
            @Override
            public void done(AVObject object, AVException e) {
//                final Shop shop = HGson.getInstance().fromJson(object.toJSONObject().toString(), Shop.class);
                AVRelation<AVObject> goods = object.getRelation("goods");
                AVQuery<AVObject> query = goods.getQuery();
                query.setLimit(10);
                query.skip(mGoods.size());
                query.findInBackground(new FindCallback<AVObject>() {
                    @Override
                    public void done(List<AVObject> list, AVException e) {
                        if (e == null) {
                            for (AVObject object : list) {
                                Good good = HGson.getInstance().fromJson(object.toJSONObject().toString(), Good.class);
                                good.setGoodUrl(object.getAVFile("pic").getUrl());
                                mGoods.add(good);
                            }
                            if (gridView.getAdapter() == null) {
                                gridView.setAdapter(mAlphaInAnimationAdapter);
                            } else {
                                mGoodsDetailAdapter.notifyDataSetChanged();
                            }
                        } else {
                            Toast.makeText(GoodsListActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });
    }
}
